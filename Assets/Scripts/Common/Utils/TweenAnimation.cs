﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Core.Enums;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace Common.Utils
{
    public class TweenAnimation : MonoBehaviour
    {
        private Sequence _animation;
        private int _currentFrame;
        private int _endFrame;
        private List<Sprite> _sprites;

        public SpriteRenderer spriteRenderer;

        protected float duration { get; set; }

        //Properties
        protected string debugTargetId => _animation.debugTargetId;
        protected float easePeriod => _animation.easePeriod;
        protected float easeOvershootOrAmplitude => _animation.easeOvershootOrAmplitude;
        protected TweenCallback<int> onWaypointChange => _animation.onWaypointChange;
        protected TweenCallback onKill => _animation.onKill;
        protected TweenCallback onComplete => _animation.onComplete;
        protected TweenCallback onStepComplete => _animation.onStepComplete;
        protected TweenCallback onUpdate => _animation.onUpdate;
        protected TweenCallback onRewind => _animation.onRewind;
        protected TweenCallback onPause => _animation.onPause;
        protected TweenCallback onPlay => _animation.onPlay;
        protected object target => _animation.target;
        protected int intId => _animation.intId;
        protected string stringId => _animation.stringId;
        protected object id => _animation.id;
        protected bool isBackwards => _animation.isBackwards;
        protected float timeScale => _animation.timeScale;

        protected void LoadSprites(string spritesPath)
        {
            try
            {
                _currentFrame = 0;
                _sprites = Resources.LoadAll<Sprite>(spritesPath).ToList();
                UpdateFrame();
                SetEndFrame();
            }
            catch (Exception e)
            {
                Debug.Log($"{_sprites} is empty./n{e}");
            }
        }

        protected void UpdateFrame()
        {
            spriteRenderer.sprite = _sprites[_currentFrame];
        }

        private void SetEndFrame()
        {
            _endFrame = _sprites.Count - 1;
        }

        protected TweenerCore<int, int, NoOptions> DoSpriteAnimation(float tweenDuration)
        {
            return DOTween.To(() => _currentFrame, x => _currentFrame = x, _endFrame, tweenDuration);
        }


        protected void CreateSequence()
        {
            _animation = DOTween.Sequence();
        }

        protected virtual void Awake()
        {
            ClearAnimation();
        }

        protected virtual void OnDestroy()
        {
            ClearAnimation();
        }

        protected void ClearAnimation()
        {
            _animation?.Kill();
            _animation = null;
        }

        //Tween features
        protected virtual void Copy()
        {
            _animation.Copy();
        }

        protected virtual void WaitForElapsedLoops(int elapsedLoops)
        {
            _animation.WaitForElapsedLoops(elapsedLoops);
        }

        protected virtual void SetSpecialStartupMode(SpecialStartupMode mode)
        {
            _animation.SetSpecialStartupMode(mode);
        }

        protected virtual void PathGetDrawPoints()
        {
            _animation.PathGetDrawPoints();
        }

        // ReSharper disable once InconsistentNaming
        protected virtual void DOTimeScale(float endValue, float scaleDuration)
        {
            _animation.DOTimeScale(endValue, scaleDuration);
        }

        protected virtual void WaitForStart()
        {
            _animation.WaitForStart();
        }

        protected virtual void WaitForRewind()
        {
            _animation.WaitForRewind();
        }

        protected virtual void WaitForPosition(float forPosition)
        {
            _animation.WaitForPosition(forPosition);
        }

        protected virtual void WaitForKill()
        {
            _animation.WaitForKill();
        }

        protected virtual void WaitForCompletion()
        {
            _animation.WaitForCompletion();
        }

        protected virtual void SetSpeedBased()
        {
            _animation.SetSpeedBased();
        }

        protected virtual void SetAutoKill()
        {
            _animation.SetAutoKill();
        }

        protected virtual void PathGetPoint(float pathPercentage)
        {
            _animation.PathGetPoint(pathPercentage);
        }

        protected virtual void OnWaypointChange(TweenCallback<int> action)
        {
            _animation.OnWaypointChange(action);
        }

        protected virtual void OnStepComplete(TweenCallback action)
        {
            _animation.OnStepComplete(action);
        }

        protected virtual void ElapsedDirectionalPercentage()
        {
            _animation.ElapsedDirectionalPercentage();
        }

        protected virtual void TogglePause()
        {
            _animation.TogglePause();
        }

        protected virtual void SmoothRewind()
        {
            _animation.SmoothRewind();
        }

        protected virtual void SetUpdate(UpdateType updateType, bool update)
        {
            _animation.SetUpdate(updateType, update);
        }

        protected virtual void SetUpdate(bool update)
        {
            _animation.SetUpdate(update);
        }

        protected virtual void SetUpdate(UpdateType updateType)
        {
            _animation.SetUpdate(updateType);
        }

        protected virtual void SetTarget(object obj)
        {
            _animation.SetTarget(obj);
        }

        protected virtual void SetRelative()
        {
            _animation.SetRelative();
        }

        protected virtual void SetRecyclable()
        {
            _animation.SetRecyclable();
        }

        protected virtual void SetLoops(int loops)
        {
            _animation.SetLoops(loops);
        }

        protected virtual void SetLink(GameObject link)
        {
            _animation.SetLink(link);
        }


        protected virtual void SetEase(Ease newEase)
        {
            _animation.SetEase(newEase);
        }

        protected virtual void SetDelay(float delay)
        {
            _animation.SetDelay(delay);
        }

        protected virtual void SetAs(TweenParams tweenParams)
        {
            _animation.SetAs(tweenParams);
        }

        protected virtual void SetAs(Tween tween)
        {
            _animation.SetAs(tween);
        }

        protected virtual void PrependInterval(float interval)
        {
            _animation.PrependInterval(interval);
        }

        protected virtual void PrependCallback(TweenCallback callback)
        {
            _animation.PrependCallback(callback);
        }

        protected virtual void PlayForward()
        {
            _animation.PlayForward();
        }

        protected virtual void PlayBackwards()
        {
            _animation.PlayBackwards();
        }

        protected virtual void PathLength()
        {
            _animation.PathLength();
        }

        protected virtual void OnUpdate(TweenCallback callback)
        {
            _animation.OnUpdate(callback);
        }

        protected virtual void OnStart(TweenCallback callback)
        {
            _animation.OnStart(callback);
        }

        protected virtual void OnRewind(TweenCallback callback)
        {
            _animation.OnRewind(callback);
        }

        protected virtual void OnPlay(TweenCallback callback)
        {
            _animation.OnPlay(callback);
        }

        protected virtual void OnPause(TweenCallback callback)
        {
            _animation.OnPause(callback);
        }


        protected virtual void OnKill(TweenCallback callback)
        {
            _animation.OnKill(callback);
        }

        protected virtual void OnComplete(TweenCallback callback)
        {
            _animation.OnComplete(callback);
        }

        protected virtual void IsPlaying()
        {
            _animation.IsPlaying();
        }

        protected virtual void IsInitialized()
        {
            _animation.IsInitialized();
        }

        protected virtual bool IsComplete()
        {
            return _animation.IsComplete();
        }

        protected virtual bool IsBackwards()
        {
            return _animation.IsBackwards();
        }

        protected virtual void IsActive()
        {
            _animation.IsActive();
        }

        protected virtual void InsertCallback(float atPosition, TweenCallback callback)
        {
            _animation.InsertCallback(atPosition, callback);
        }

        protected virtual void GotoWaypoint(int waypointIndex, bool andPlay)
        {
            _animation.GotoWaypoint(waypointIndex, andPlay);
        }

        protected virtual void ForceInit()
        {
            _animation.ForceInit();
        }

        protected virtual void ElapsedPercentage()
        {
            _animation.ElapsedPercentage();
        }

        protected virtual void CompletedLoops()
        {
            _animation.CompletedLoops();
        }

        protected virtual void AppendInterval(float interval)
        {
            _animation.AppendInterval(interval);
        }

        protected virtual void AppendCallback(TweenCallback callback)
        {
            _animation.AppendCallback(callback);
        }

        protected virtual void Rewind()
        {
            _animation.Rewind();
        }

        protected virtual void Restart()
        {
            _animation.Restart();
        }

        protected virtual void Prepend(Tween tween)
        {
            _animation.Prepend(tween);
        }

        protected virtual void Play()
        {
            _animation.Play();
        }

        protected virtual void Pause()
        {
            _animation.Pause();
        }

        protected virtual void Loops()
        {
            _animation.Loops();
        }

        protected virtual void Kill()
        {
            _animation.Kill();
        }

        protected virtual void Join(Tween tween)
        {
            _animation.Join(tween);
        }

        protected virtual void Insert(float atPosition, Tween tween)
        {
            _animation.Insert(atPosition, tween);
        }

        protected virtual void Goto(float to, bool andPlay)
        {
            _animation.Goto(to, andPlay);
        }

        protected virtual void Flip()
        {
            _animation.Flip();
        }

        protected virtual void Elapsed()
        {
            _animation.Elapsed();
        }

        protected virtual void Duration()
        {
            _animation.Duration();
        }

        protected virtual void Delay()
        {
            _animation.Delay();
        }

        protected virtual void Append(Tween tween)
        {
            _animation.Append(tween);
        }

        protected virtual void Complete()
        {
            _animation.Complete();
        }
    }
}