﻿namespace Common.Enums
{
    public enum EnemyType
    {
        Archmage,
        Werewolf,
        Demon
    }
}