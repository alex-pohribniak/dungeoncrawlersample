﻿namespace Common.Enums
{
    public enum HeroType
    {
        Warrior,
        Rogue,
        Wizard,
        Cleric
    }
}