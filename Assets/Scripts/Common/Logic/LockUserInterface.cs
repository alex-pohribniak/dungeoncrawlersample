﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace Common.Logic
{
    public class LockUserInterface : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(CommonEvent.LockUserInterface);
            Complete();
        }
    }
}