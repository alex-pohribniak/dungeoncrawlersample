﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace Common.Logic
{
    public class UnLockUserInterface : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(CommonEvent.UnLockUserInterface);
            Complete();
        }
    }
}