﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace Common.Logic
{
    public class ZoomCamera : BaseTask
    {
        private readonly SimpleDelegate _onZoomCameraComplete;

        public ZoomCamera()
        {
            _onZoomCameraComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(CommonEvent.ZoomCamera);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(CommonEvent.ZoomCameraComplete, _onZoomCameraComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(CommonEvent.ZoomCameraComplete, _onZoomCameraComplete);
        }
    }
}