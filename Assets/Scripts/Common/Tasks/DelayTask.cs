﻿using DG.Tweening;

namespace Common.Tasks
{
    public class DelayTask : BaseTask
    {
        private Sequence _delayTween;
        public int delay;

        protected override void OnExecute()
        {
            base.OnExecute();
            DelayComplete();
        }

        private void DelayComplete()
        {
            _delayTween?.Kill();
            _delayTween = DOTween.Sequence();
            _delayTween.onComplete = Complete;
            _delayTween.SetDelay(delay / 1000f);
            _delayTween.Play();
        }

        public override void Complete()
        {
            ClearDelay();
            base.Complete();
        }

        protected internal override void OnTerminate()
        {
            ClearDelay();
            base.OnTerminate();
        }

        private void ClearDelay()
        {
            _delayTween?.Kill();
            _delayTween = null;
        }

        private void OnDestroy()
        {
            _delayTween?.Kill();
            _delayTween = null;
        }
    }
}