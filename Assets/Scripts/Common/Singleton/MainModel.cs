﻿using System;
using BattleSample.Model;
using DungeonCrawlerSample.Model;
using Game.Model;
using UnityEngine;

namespace Common.Singleton
{
    public class MainModel : MonoBehaviour
    {
        private static MainModel _mainModel;
        private BattleSampleData _battleSampleData;
        private ComplexDungeonCrawlerSampleData _complexDungeonCrawlerSampleData;
        private PrimitiveDungeonCrawlerSampleData _primitiveDungeonCrawlerSampleData;

        private static MainModel Instance
        {
            get
            {
                if (_mainModel != null) return _mainModel;
                _mainModel = FindObjectOfType(typeof(MainModel)) as MainModel;
                if (_mainModel == null)
                    throw new Exception("There needs to be one active Model script in your scene.");
                _mainModel.Init();

                return _mainModel;
            }
        }

        public static ComplexDungeonCrawlerSampleData ComplexDungeonCrawlerSampleData
        {
            get => Instance._complexDungeonCrawlerSampleData;
            set => Instance._complexDungeonCrawlerSampleData = value;
        }

        public static PrimitiveDungeonCrawlerSampleData PrimitiveDungeonCrawlerSampleData
        {
            get => Instance._primitiveDungeonCrawlerSampleData;
            set => Instance._primitiveDungeonCrawlerSampleData = value;
        }


        public static BattleSampleData BattleSampleData
        {
            get => Instance._battleSampleData;
            set => Instance._battleSampleData = value;
        }


        public static bool IsNull()
        {
            return _mainModel == null;
        }

        private void Init()
        {
            if (_complexDungeonCrawlerSampleData == null) _complexDungeonCrawlerSampleData = new ComplexDungeonCrawlerSampleData();
            if (_primitiveDungeonCrawlerSampleData == null) _primitiveDungeonCrawlerSampleData = new PrimitiveDungeonCrawlerSampleData();
            if (_battleSampleData == null) _battleSampleData = new BattleSampleData();
        }
    }
}