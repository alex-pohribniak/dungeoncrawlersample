﻿using Cinemachine;
using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using UnityEngine;

namespace Common.View
{
    public class ZoomCameraView : MonoBehaviour
    {
        public CinemachineVirtualCamera virtualCamera;

        private readonly SimpleDelegate _onZoomCamera;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public ZoomCameraView()
        {
            _onZoomCamera = OnZoomCamera;
        }

        protected void Awake()
        {
            Observer.AddListener(CommonEvent.ZoomCamera, _onZoomCamera);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(CommonEvent.ZoomCamera, _onZoomCamera);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnZoomCamera()
        {
            if (MainModel.PrimitiveDungeonCrawlerSampleData.encounterEnemy)
                ZoomIn();
            else
                ZoomOut();
        }

        private void ZoomIn()
        {
            ClearTween();
            _tweenDuration = 2f;
            _tweenAnimation = DOTween.Sequence();
            var tweenZoom = DOTween.To(size => virtualCamera.m_Lens.OrthographicSize = size,
                virtualCamera.m_Lens.OrthographicSize, virtualCamera.m_Lens.OrthographicSize / 2, _tweenDuration);
            _tweenAnimation.Join(tweenZoom);
            _tweenAnimation.OnComplete(() => { Observer.Emit(CommonEvent.ZoomCameraComplete); });
            _tweenAnimation.Play();
        }

        private void ZoomOut()
        {
            ClearTween();
            _tweenDuration = 2f;
            _tweenAnimation = DOTween.Sequence();
            var tweenZoom = DOTween.To(size => virtualCamera.m_Lens.OrthographicSize = size,
                virtualCamera.m_Lens.OrthographicSize / 2, virtualCamera.m_Lens.OrthographicSize, _tweenDuration);
            _tweenAnimation.Join(tweenZoom);
            _tweenAnimation.OnComplete(() => { Observer.Emit(CommonEvent.ZoomCameraComplete); });
            _tweenAnimation.Play();
        }
    }
}