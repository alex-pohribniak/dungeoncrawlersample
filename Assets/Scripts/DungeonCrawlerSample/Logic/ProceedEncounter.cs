﻿using Common.Singleton;
using Common.Tasks;
using UnityEngine.SceneManagement;

namespace DungeonCrawlerSample.Logic
{
    public class ProceedEncounter : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Complete();
            if (MainModel.PrimitiveDungeonCrawlerSampleData.encounterEnemy) SceneManager.LoadScene("BattleSample");
        }

        protected override bool Guard()
        {
            return MainModel.PrimitiveDungeonCrawlerSampleData.encounterEnemy;
        }
    }
}