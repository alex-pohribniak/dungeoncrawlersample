﻿using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Config;

namespace DungeonCrawlerSample.Logic
{
    public class UpdateEnemyPosition : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var complexEnemies = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies;
            var primitiveEnemies = MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEnemies;
            foreach (var primitiveEnemy in primitiveEnemies)
            {
                if (primitiveEnemy.killed) continue;
                if (primitiveEnemy.idleDuration > 0)
                {
                    primitiveEnemy.idleDuration--;
                    continue;
                }

                if (primitiveEnemy.revertPath)
                {
                    primitiveEnemy.currentWaypoint--;
                    if (primitiveEnemy.currentWaypoint > 0) continue;
                    primitiveEnemy.revertPath = false;
                    primitiveEnemy.currentWaypoint = 0;
                    primitiveEnemy.idleDuration = EnemyConfig.IdleDuration;
                }
                else
                {
                    primitiveEnemy.currentWaypoint++;
                    if (primitiveEnemy.currentWaypoint <
                        complexEnemies.Find(enemy => enemy.id == primitiveEnemy.id).path.vectorPath.Count) continue;
                    primitiveEnemy.revertPath = true;
                    primitiveEnemy.currentWaypoint =
                        complexEnemies.Find(enemy => enemy.id == primitiveEnemy.id).path.vectorPath.Count - 1;
                    primitiveEnemy.idleDuration = EnemyConfig.IdleDuration;
                }
            }

            Complete();
        }
    }
}