﻿using System;
using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Config;
using UnityEngine;
using UnityEngine.EventSystems;

namespace DungeonCrawlerSample.Logic
{
    public class PlayerController : MonoBehaviour, IPointerDownHandler
    {
        public Camera mainCamera;
        public BaseTask turn;

        public void OnPointerDown(PointerEventData eventData)
        {
            var pointerPosition = mainCamera.ScreenToWorldPoint(eventData.position);
            DefineDirection(pointerPosition);
            turn.Execute();
        }

        private static void DefineDirection(Vector3 pointerPosition)
        {
            var pointerX = pointerPosition.x;
            var pointerY = pointerPosition.y;
            var playerX = MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.currentPosition.x;
            var playerY = MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.currentPosition.y;
            if (Math.Abs(Math.Abs(pointerX) - Math.Abs(playerX)) > Math.Abs(Math.Abs(pointerY) - Math.Abs(playerY)))
            {
                if (pointerX > playerX)
                    MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.moveDirection = PlayerConfig.MoveRight;
                else if (pointerX < playerX)
                    MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.moveDirection = PlayerConfig.MoveLeft;
            }
            else if (Math.Abs(Math.Abs(pointerX) - Math.Abs(playerX)) <
                     Math.Abs(Math.Abs(pointerY) - Math.Abs(playerY)))
            {
                if (pointerY > playerY)
                    MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.moveDirection = PlayerConfig.MoveTop;
                else if (pointerY < playerY)
                    MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.moveDirection = PlayerConfig.MoveBot;
            }
        }
    }
}