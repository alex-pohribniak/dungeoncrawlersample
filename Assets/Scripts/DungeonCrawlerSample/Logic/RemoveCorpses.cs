﻿using System.Collections.Generic;
using System.Linq;
using Common.Singleton;
using Common.Tasks;
using Game.Model;

namespace DungeonCrawlerSample.Logic
{
    public class RemoveCorpses : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var complexEnemies = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies;
            var primitiveEnemies = MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEnemies;
            var primitiveEncounterEnemy = MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEncounterEnemy;

            for (var i = 0; i < primitiveEnemies.Count; i++)
                if (primitiveEnemies[i].id == complexEnemies[i].id &&
                    primitiveEnemies[i].id == primitiveEncounterEnemy.id)
                    primitiveEnemies[i].killed = primitiveEncounterEnemy.killed;

            if (primitiveEnemies.All(enemy => enemy.killed))
            {
                DestroyComplexEnemies();
                Complete();
                return;
            }

            var complexCorpses = new List<ComplexEnemyData>();
            for (var i = 0; i < primitiveEnemies.Count; i++)
                if (primitiveEnemies[i].id == complexEnemies[i].id && primitiveEnemies[i].killed)
                    complexCorpses.Add(complexEnemies[i]);

            foreach (var complexCorpse in complexCorpses)
            {
                complexEnemies.Remove(complexCorpse);
                Destroy(complexCorpse.enemyObject);
            }

            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.PrimitiveDungeonCrawlerSampleData.needLoad;
        }

        private static void DestroyComplexEnemies()
        {
            var complexEnemies = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies;
            if (complexEnemies.Count == 0)
                return;
            foreach (var complexEnemy in complexEnemies) Destroy(complexEnemy.enemyObject);
            MainModel.ComplexDungeonCrawlerSampleData.complexEnemies.Clear();
        }
    }
}