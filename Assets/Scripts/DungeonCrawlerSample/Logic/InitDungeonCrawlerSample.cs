﻿using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Config;
using UnityEngine;

namespace DungeonCrawlerSample.Logic
{
    public class InitDungeonCrawlerSample : BaseTask
    {
        public DungeonCrawlerSampleConfig dungeonCrawlerSampleConfig;

        protected override void OnExecute()
        {
            base.OnExecute();
            Physics2D.gravity = Vector2.zero;
            MainModel.ComplexDungeonCrawlerSampleData = dungeonCrawlerSampleConfig.complexDungeonCrawlerSampleData;
            MainModel.PrimitiveDungeonCrawlerSampleData = dungeonCrawlerSampleConfig.primitiveDungeonCrawlerSampleData;
            MainModel.ComplexDungeonCrawlerSampleData.complexPlayer.playerObject.transform.position =
                MainModel.PrimitiveDungeonCrawlerSampleData.playerSpawnPosition;
            MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.currentPosition =
                MainModel.PrimitiveDungeonCrawlerSampleData.playerSpawnPosition;
            Complete();
        }
    }
}