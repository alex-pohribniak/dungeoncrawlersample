﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Enums;

namespace DungeonCrawlerSample.Logic
{
    public class EnemiesMove : BaseTask
    {
        private readonly SimpleDelegate _onEnemiesMoveComplete;

        public EnemiesMove()
        {
            _onEnemiesMoveComplete = OnEnemiesMoveComplete;
        }

        private void OnEnemiesMoveComplete()
        {
            var primitiveEnemies = MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEnemies;
            var complexEnemies = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies;
            for (var i = 0; i < primitiveEnemies.Count; i++)
            for (var j = 0; j < complexEnemies.Count; j++)
                if (primitiveEnemies[i].id == complexEnemies[j].id)
                    MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEnemies[i].currentPosition =
                        MainModel.ComplexDungeonCrawlerSampleData.complexEnemies[j].enemyObject.transform.position;

            Complete();
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(DungeonCrawlerSampleEvent.EnemiesMove);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(DungeonCrawlerSampleEvent.EnemiesMoveComplete, _onEnemiesMoveComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(DungeonCrawlerSampleEvent.EnemiesMoveComplete, _onEnemiesMoveComplete);
        }
    }
}