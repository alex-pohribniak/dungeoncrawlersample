﻿using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Enums;

namespace DungeonCrawlerSample.Logic
{
    public class UpdateView : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(DungeonCrawlerSampleEvent.UpdateView);
            Complete();
        }
    }
}