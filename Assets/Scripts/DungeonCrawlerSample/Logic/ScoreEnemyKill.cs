﻿using Common.Singleton;
using Common.Tasks;

namespace DungeonCrawlerSample.Logic
{
    public class ScoreEnemyKill : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.killedEnemies++;
            MainModel.PrimitiveDungeonCrawlerSampleData.encounterEnemy = false;
            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.PrimitiveDungeonCrawlerSampleData.encounterEnemy;
        }
    }
}