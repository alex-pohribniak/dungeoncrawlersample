﻿using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Model;
using Game.Model;
using UnityEngine;

namespace DungeonCrawlerSample.Logic
{
    public class CheckEncounterEnemy : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var grid = MainModel.ComplexDungeonCrawlerSampleData.background.layoutGrid;
            var player = MainModel.ComplexDungeonCrawlerSampleData.complexPlayer;
            var playerCell = grid.WorldToCell(player.playerObject.transform.position);
            var complexEnemies = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies;
            var primitiveEnemies = MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEnemies;

            foreach (var complexEnemy in complexEnemies)
            {
                var enemyCell = grid.WorldToCell(complexEnemy.enemyObject.transform.position);
                var primitiveEnemy = primitiveEnemies.Find(enemy => enemy.id == complexEnemy.id);
                
                if (playerCell == enemyCell)
                    Encounter(primitiveEnemy);
                else if (playerCell + new Vector3Int(0, 1, 0) == enemyCell)
                    Encounter(primitiveEnemy);
                else if (playerCell + new Vector3Int(0, -1, 0) == enemyCell)
                    Encounter(primitiveEnemy);
                else if (playerCell + new Vector3Int(1, 0, 0) == enemyCell)
                    Encounter(primitiveEnemy);
                else if (playerCell + new Vector3Int(-1, 0, 0) == enemyCell)
                    Encounter(primitiveEnemy);
            }

            Complete();
        }

        private static void Encounter(PrimitiveEnemyData primitiveEncounterEnemy)
        {
            MainModel.PrimitiveDungeonCrawlerSampleData.encounterEnemy = true;
            MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEncounterEnemy = primitiveEncounterEnemy;
        }
    }
}