﻿using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Enums;

namespace DungeonCrawlerSample.Logic
{
    public class RestoreDungeonCrawlerSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(DungeonCrawlerSampleEvent.RestoreDungeonCrawlerSample);
            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.PrimitiveDungeonCrawlerSampleData.needLoad;
        }
    }
}