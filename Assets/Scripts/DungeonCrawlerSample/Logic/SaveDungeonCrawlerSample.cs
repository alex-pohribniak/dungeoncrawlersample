﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using UnityEngine;

namespace DungeonCrawlerSample.Logic
{
    public class SaveDungeonCrawlerSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            PlayerPrefs.SetString(JsonKeys.PrimitiveDungeonCrawlerSampleData,
                JsonUtility.ToJson(MainModel.PrimitiveDungeonCrawlerSampleData));
            PlayerPrefs.Save();
            Complete();
        }
    }
}