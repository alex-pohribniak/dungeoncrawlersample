﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Enums;
using DungeonCrawlerSample.Model;
using Game.Model;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DungeonCrawlerSample.Logic
{
    public class DungeonCrawlerSampleLogic : MonoBehaviour
    {
        private readonly SimpleDelegate _onRestartGame;
        
        public BaseTask startTask;

        public DungeonCrawlerSampleLogic()
        {
            _onRestartGame = OnRestartGame;
        }

        private void OnRestartGame()
        {
            ResetRestore();
            SceneManager.LoadScene("DungeonCrawlerSample");
        }

        private static void ResetRestore()
        {
            var primitiveGameData =
                JsonUtility.FromJson<PrimitiveDungeonCrawlerSampleData>(PlayerPrefs.GetString(JsonKeys.PrimitiveDungeonCrawlerSampleData));
            primitiveGameData.needLoad = false;
            PlayerPrefs.SetString(JsonKeys.PrimitiveDungeonCrawlerSampleData, JsonUtility.ToJson(primitiveGameData));
            PlayerPrefs.Save();
        }

        private void Start()
        {
            startTask.Execute();
        }

        private void Awake()
        {
            Observer.AddListener(DungeonCrawlerSampleEvent.RestartDungeonCrawlerSample, _onRestartGame);
        }

        private void OnDestroy()
        {
            ResetRestore();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(DungeonCrawlerSampleEvent.RestartDungeonCrawlerSample, _onRestartGame);
        }
    }
}