﻿using Common.Singleton;
using Common.Tasks;

namespace DungeonCrawlerSample.Logic
{
    public class EncounterParallel : ParallelTask
    {
        protected override bool Guard()
        {
            return MainModel.PrimitiveDungeonCrawlerSampleData.encounterEnemy;
        }
    }
}