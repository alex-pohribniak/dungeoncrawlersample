﻿using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Enums;

namespace DungeonCrawlerSample.Logic
{
    public class CheckWin : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(DungeonCrawlerSampleEvent.ShowResult);
            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.killedEnemies == 3;
        }
    }
}