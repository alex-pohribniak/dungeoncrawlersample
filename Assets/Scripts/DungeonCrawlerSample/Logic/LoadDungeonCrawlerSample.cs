﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Model;
using Game.Model;
using UnityEngine;

namespace DungeonCrawlerSample.Logic
{
    public class LoadDungeonCrawlerSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var loadData =
                JsonUtility.FromJson<PrimitiveDungeonCrawlerSampleData>(
                    PlayerPrefs.GetString(JsonKeys.PrimitiveDungeonCrawlerSampleData));
            if (loadData == null)
            {
                Complete();
                return;
            }

            if (loadData.needLoad)
            {
                MainModel.PrimitiveDungeonCrawlerSampleData = loadData;
                Complete();
            }
            else
            {
                Complete();
            }
        }
    }
}