﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Enums;

namespace DungeonCrawlerSample.Logic
{
    public class PlayerMove : BaseTask
    {
        private readonly SimpleDelegate _onPlayerMoveComplete;

        public PlayerMove()
        {
            _onPlayerMoveComplete = OnPlayerMoveComplete;
        }

        private void OnPlayerMoveComplete()
        {
            MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.currentPosition =
                MainModel.ComplexDungeonCrawlerSampleData.complexPlayer.playerObject.transform.position;
            Complete();
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(DungeonCrawlerSampleEvent.PlayerMove);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(DungeonCrawlerSampleEvent.PlayerMoveComplete, _onPlayerMoveComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.AddListener(DungeonCrawlerSampleEvent.PlayerMoveComplete, _onPlayerMoveComplete);
        }
    }
}