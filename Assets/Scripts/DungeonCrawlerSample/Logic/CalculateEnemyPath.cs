﻿using Common.Singleton;
using Common.Tasks;
using Pathfinding;

namespace DungeonCrawlerSample.Logic
{
    public class CalculateEnemyPath : BaseTask
    {
        private int _enemyCounter;

        protected override void OnExecute()
        {
            base.OnExecute();
            _enemyCounter = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies.Count;
            var complexEnemies = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies;
            var primitiveEnemies = MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEnemies;
            for (var i = 0; i < complexEnemies.Count; i++)
            {
                var enemySeeker = complexEnemies[i].enemyObject.GetComponent<Seeker>();
                if (!enemySeeker.IsDone()) continue;
                var index = i;
                enemySeeker.StartPath(primitiveEnemies[i].startPath, primitiveEnemies[i].endPath, path =>
                {
                    if (path.error) return;
                    complexEnemies[index].path = path;
                    primitiveEnemies[index].currentWaypoint = 0;

                    _enemyCounter--;
                    if (_enemyCounter == 0) Complete();
                });
            }
        }
    }
}