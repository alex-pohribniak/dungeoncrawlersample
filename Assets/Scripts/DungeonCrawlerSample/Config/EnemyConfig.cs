﻿namespace DungeonCrawlerSample.Config
{
    public static class EnemyConfig
    {
        public const float MoveDuration = 0.5f;
        public const float InstantMove = 0f;
        public const int IdleDuration = 3;
    }
}