﻿using UnityEngine;

namespace DungeonCrawlerSample.Config
{
    public static class PlayerConfig
    {
        public const float MoveDuration = 0.5f;
        public const float InstantMove = 0f;
        public static readonly Vector3Int MoveTop = new Vector3Int(0, 16, 0);
        public static readonly Vector3Int MoveBot = new Vector3Int(0, -16, 0);
        public static readonly Vector3Int MoveRight = new Vector3Int(16, 0, 0);
        public static readonly Vector3Int MoveLeft = new Vector3Int(-16, 0, 0);
    }
}