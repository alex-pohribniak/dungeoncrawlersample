﻿using DungeonCrawlerSample.Model;
using Game.Model;
using UnityEngine;

namespace DungeonCrawlerSample.Config
{
    public class DungeonCrawlerSampleConfig : MonoBehaviour
    {
        public ComplexDungeonCrawlerSampleData complexDungeonCrawlerSampleData;
        public PrimitiveDungeonCrawlerSampleData primitiveDungeonCrawlerSampleData;
    }
}