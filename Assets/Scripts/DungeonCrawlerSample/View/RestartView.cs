﻿using Common.Singleton;
using DungeonCrawlerSample.Enums;
using UnityEngine;
using Object = System.Object;

namespace Game.View
{
    public class RestartView : MonoBehaviour
    {
        public void OnClick()
        {
            Observer.Emit(DungeonCrawlerSampleEvent.RestartDungeonCrawlerSample);
        }
    }
}