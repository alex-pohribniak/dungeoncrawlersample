﻿using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using DungeonCrawlerSample.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonCrawlerSample.View
{
    public class ResultView : MonoBehaviour
    {
        public Image gameResult;
        public Button restart;

        private readonly SimpleDelegate _onShowGameResult;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public ResultView()
        {
            _onShowGameResult = OnShowGameResult;
        }

        protected void Awake()
        {
            gameResult.gameObject.SetActive(false);
            restart.gameObject.SetActive(false);
            Observer.AddListener(DungeonCrawlerSampleEvent.ShowResult, _onShowGameResult);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(DungeonCrawlerSampleEvent.ShowResult, _onShowGameResult);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnShowGameResult()
        {
            gameResult.gameObject.SetActive(true);
            ClearTween();
            _tweenDuration = 3f;
            _tweenAnimation = DOTween.Sequence();
            var tweenFade0 = gameResult.DOFade(1f, _tweenDuration);
            _tweenAnimation.Join(tweenFade0);
            _tweenAnimation.OnComplete(() =>
            {
                restart.gameObject.SetActive(true);
                Observer.Emit(DungeonCrawlerSampleEvent.ShowResultComplete);
            });
            _tweenAnimation.Play();
        }
    }
}