﻿using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using DungeonCrawlerSample.Config;
using DungeonCrawlerSample.Enums;
using UnityEngine;

namespace Game.View
{
    public class EnemiesMoveAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onEnemiesMove;
        private readonly SimpleDelegate _onRestoreGame;

        public EnemiesMoveAnimation()
        {
            _onRestoreGame = OnRestoreGame;
            _onEnemiesMove = OnEnemiesMove;
        }

        private void OnRestoreGame()
        {
            var grid = MainModel.ComplexDungeonCrawlerSampleData.background.layoutGrid;
            var complexEnemies = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies;
            var primitiveEnemies = MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEnemies;

            if (complexEnemies.Count == 0) return;

            foreach (var complexEnemy in complexEnemies)
            {
                var enemyTransform = complexEnemy.enemyObject.transform;
                var targetPosition = primitiveEnemies.Find(enemy => enemy.id == complexEnemy.id).currentPosition;
                var targetCell = grid.WorldToCell(targetPosition);
                var targetCellCenter = grid.GetCellCenterWorld(targetCell);
                var tween = enemyTransform.DOMove(targetCellCenter, EnemyConfig.InstantMove);
                tween.Play();
            }
        }

        private void OnEnemiesMove()
        {
            ClearAnimation();
            CreateSequence();
            var complexEnemies = MainModel.ComplexDungeonCrawlerSampleData.complexEnemies;
            var primitiveEnemies = MainModel.PrimitiveDungeonCrawlerSampleData.primitiveEnemies;

            if (complexEnemies.Count == 0)
            {
                Observer.Emit(DungeonCrawlerSampleEvent.EnemiesMoveComplete);
                return;
            }

            foreach (var complexEnemy in complexEnemies)
            {
                var enemyRigidBody = complexEnemy.enemyObject.GetComponent<Rigidbody2D>();
                var targetPosition =
                    (Vector2) complexEnemy.path.vectorPath[
                        primitiveEnemies.Find(enemy => enemy.id == complexEnemy.id).currentWaypoint] -
                    enemyRigidBody.position.normalized;
                var tween = enemyRigidBody.DOMove(targetPosition, EnemyConfig.MoveDuration);
                Join(tween);
            }

            OnComplete(() =>
            {
                ClearAnimation();
                Observer.Emit(DungeonCrawlerSampleEvent.EnemiesMoveComplete);
            });
            Play();
        }

        protected override void Awake()
        {
            base.Awake();
            Observer.AddListener(DungeonCrawlerSampleEvent.EnemiesMove, _onEnemiesMove);
            Observer.AddListener(DungeonCrawlerSampleEvent.RestoreDungeonCrawlerSample, _onRestoreGame);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(DungeonCrawlerSampleEvent.EnemiesMove, _onEnemiesMove);
            Observer.RemoveListener(DungeonCrawlerSampleEvent.RestoreDungeonCrawlerSample, _onRestoreGame);
        }
    }
}