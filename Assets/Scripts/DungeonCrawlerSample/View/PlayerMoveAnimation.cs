﻿using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using DungeonCrawlerSample.Config;
using DungeonCrawlerSample.Enums;
using UnityEngine;

namespace Game.View
{
    public class PlayerMoveAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayerMove;
        private readonly SimpleDelegate _onRestoreGame;

        public PlayerMoveAnimation()
        {
            _onRestoreGame = OnRestoreGame;
            _onPlayerMove = OnPlayerMove;
        }

        private void OnRestoreGame()
        {
            var grid = MainModel.ComplexDungeonCrawlerSampleData.background.layoutGrid;
            var playerTransform = MainModel.ComplexDungeonCrawlerSampleData.complexPlayer.playerObject.transform;
            var targetPosition = MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.currentPosition;
            var targetCell = grid.WorldToCell(targetPosition);
            var targetCellCenter = grid.GetCellCenterWorld(targetCell);
            var tween = playerTransform.DOMove(targetCellCenter, PlayerConfig.InstantMove);
            tween.Play();
        }

        private void OnPlayerMove()
        {
            ClearAnimation();
            CreateSequence();
            var playerRigidBody = MainModel.ComplexDungeonCrawlerSampleData.complexPlayer.playerObject.GetComponent<Rigidbody2D>();
            var playerPosition = MainModel.ComplexDungeonCrawlerSampleData.complexPlayer.playerObject.transform.position;
            var targetPosition = playerPosition + MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.moveDirection;
            var tween = playerRigidBody.DOMove(targetPosition, PlayerConfig.MoveDuration);
            Append(tween);
            OnComplete(() =>
            {
                ClearAnimation();
                Observer.Emit(DungeonCrawlerSampleEvent.PlayerMoveComplete);
            });
            Play();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            PlayBackwards();
            Observer.Emit(DungeonCrawlerSampleEvent.PlayerMoveComplete);
        }

        protected override void Awake()
        {
            base.Awake();
            Observer.AddListener(DungeonCrawlerSampleEvent.PlayerMove, _onPlayerMove);
            Observer.AddListener(DungeonCrawlerSampleEvent.RestoreDungeonCrawlerSample, _onRestoreGame);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(DungeonCrawlerSampleEvent.PlayerMove, _onPlayerMove);
            Observer.RemoveListener(DungeonCrawlerSampleEvent.RestoreDungeonCrawlerSample, _onRestoreGame);
        }
    }
}