﻿using Common.Enums;
using Common.Singleton;
using DungeonCrawlerSample.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace DungeonCrawlerSample.View
{
    public class CountersView : MonoBehaviour
    {
        private readonly SimpleDelegate _onUpdateView;

        public Text killedEnemiesCount;

        public CountersView()
        {
            _onUpdateView = OnUpdateView;
        }

        private void OnUpdateView()
        {
            killedEnemiesCount.text = MainModel.PrimitiveDungeonCrawlerSampleData.primitivePlayer.killedEnemies.ToString();
        }

        private void Awake()
        {
            Observer.AddListener(DungeonCrawlerSampleEvent.UpdateView, _onUpdateView);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(DungeonCrawlerSampleEvent.UpdateView, _onUpdateView);
        }
    }
}