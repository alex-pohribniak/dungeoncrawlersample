﻿using System;
using System.Collections.Generic;
using BattleSample.Model;
using UnityEngine;

namespace DungeonCrawlerSample.Model
{
    [Serializable]
    public class PrimitivePlayerData
    {
        public List<BattleSampleHeroData> battleSampleHeroesData;
        
        public bool alive;
        public int killedEnemies;
        
        public Vector3 currentPosition;
        public Vector3Int moveDirection;
    }
}