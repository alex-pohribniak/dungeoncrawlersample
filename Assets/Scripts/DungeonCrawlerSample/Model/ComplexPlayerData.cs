﻿using System;
using UnityEngine;

namespace Game.Model
{
    [Serializable]
    public class ComplexPlayerData
    {
        public GameObject playerObject;
    }
}