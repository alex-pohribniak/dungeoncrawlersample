﻿using System;
using System.Collections.Generic;
using Game.Model;
using UnityEngine;

namespace DungeonCrawlerSample.Model
{
    [Serializable]
    public class PrimitiveDungeonCrawlerSampleData
    {
        public PrimitivePlayerData primitivePlayer;
        public List<PrimitiveEnemyData> primitiveEnemies;
        public PrimitiveEnemyData primitiveEncounterEnemy;
        
        public Vector3Int playerSpawnPosition;

        public bool encounterEnemy;
        public bool needLoad;
    }
}