﻿using System;
using Pathfinding;
using UnityEngine;

namespace Game.Model
{
    [Serializable]
    public class ComplexEnemyData
    {
        public int id;
        
        public GameObject enemyObject;
        public Path path;
    }
}