﻿using System;
using System.Collections.Generic;
using BattleSample.Model;
using Common.Enums;
using UnityEngine;

namespace DungeonCrawlerSample.Model
{
    [Serializable]
    public class PrimitiveEnemyData
    {
        public List<BattleSampleEnemyData> battleSampleEnemiesData;
        
        public int id;
        
        public bool alive;
        public bool killed;
        public bool revertPath;

        public Vector3Int startPath;
        public Vector3Int endPath;
        public Vector3 currentPosition;
        
        public int currentWaypoint;
        public int idleDuration;
        
        public EnemyType enemyType;
    }
}