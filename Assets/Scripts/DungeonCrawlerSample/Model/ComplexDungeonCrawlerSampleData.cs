﻿using System;
using System.Collections.Generic;
using UnityEngine.Tilemaps;

namespace Game.Model
{
    [Serializable]
    public class ComplexDungeonCrawlerSampleData
    {
        public Tilemap background;
        public Tilemap walls;
        public Tilemap terrain;
        public Tilemap bridges;
        public Tilemap objects;
        public Tilemap notCollidableObjects;

        public ComplexPlayerData complexPlayer;
        public List<ComplexEnemyData> complexEnemies;
    }
}