﻿namespace DungeonCrawlerSample.Enums
{
    public static class DungeonCrawlerSampleEvent
    {
        public const string PlayerMove = "PlayerMove";
        public const string PlayerMoveComplete = "PlayerMoveComplete";
        public const string EnemiesMove = "EnemiesMove";
        public const string EnemiesMoveComplete = "EnemiesMoveComplete";
        public const string UpdateView = "UpdateView";
        public const string ShowResult = "ShowResult";
        public const string ShowResultComplete = "ShowResultComplete";
        public const string RestoreDungeonCrawlerSample = "RestoreDungeonCrawlerSample";
        public const string RestartDungeonCrawlerSample = "RestartDungeonCrawlerSample";
    }
}