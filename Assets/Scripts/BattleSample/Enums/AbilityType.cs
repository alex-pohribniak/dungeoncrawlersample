﻿namespace BattleSample.Enums
{
    public enum AbilityType
    {
        //Common
        HeroSimpleAttack,
        //Heroes
        Defence,
        Taunt,
        ArmorBreak,
        FireBall,
        Heal,
        //Enemies
        EnemySimpleAttack,
        EnemyBleedAttack,
        EnemyDevourAttack,
        EnemyMassAttack,
    }
}