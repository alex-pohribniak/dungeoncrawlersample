﻿namespace BattleSample.Enums
{
    public static class BattleSampleEvent
    {
        public const string PlayerKilled = "PlayerKilled";
        public const string EnemyKilled = "EnemyKilled";

        public const string RevealEnemyCharacters = "RevealEnemyCharacters";

        public const string OnHeroAbilitySelect = "OnHeroAbilitySelect";
        public const string OnHeroSelect = "OnHeroSelect";
        public const string OnEnemySelect = "OnEnemySelect";

        public const string UpdateHeroesView = "UpdateHeroesView";
        public const string UpdateEnemiesView = "UpdateEnemiesView";
        public const string UpdateEnemyCharactersView = "UpdateEnemyCharactersView";

        public const string ShowResult = "ShowResult";
        public const string ShowResultComplete = "ShowResultComplete";

        public const string PlayHeroOffensiveAbilityAnimation = "PlayHeroOffensiveAbilityAnimation";
        public const string PlayHeroOffensiveAbilityAnimationComplete = "PlayHeroOffensiveAbilityAnimationComplete";

        public const string PlayEnemyOffensiveAbilityAnimation = "PlayEnemyOffensiveAbilityAnimation";
        public const string PlayEnemyOffensiveAbilityAnimationComplete = "PlayEnemyOffensiveAbilityAnimationComplete";

        public const string PlayHeroSimpleAttackAnimation = "PlayHeroSimpleAttackAnimation";
        public const string PlayHeroSimpleAttackAnimationComplete = "PlayHeroSimpleAttackAnimationComplete";
        public const string PlayDefenceAnimation = "PlayDefenceAnimation";
        public const string PlayDefenceAnimationComplete = "PlayDefenceAnimationComplete";
        public const string PlayTauntAnimation = "PlayTauntAnimation";
        public const string PlayTauntAnimationComplete = "PlayTauntAnimationComplete";
        public const string PlayArmorBreakAnimation = "PlayArmorBreakAnimation";
        public const string PlayArmorBreakAnimationComplete = "PlayArmorBreakAnimationComplete";
        public const string PlayFireBallAnimation = "PlayFireBallAnimation";
        public const string PlayFireBallAnimationComplete = "PlayFireBallAnimationComplete";
        public const string PlayHealAnimation = "PlayHealAnimation";
        public const string PlayHealAnimationComplete = "PlayHealAnimationComplete";

        public const string PlayEnemySimpleAttackAnimation = "PlayEnemySimpleAttackAnimation";
        public const string PlayEnemySimpleAttackAnimationComplete = "PlayEnemySimpleAttackAnimationComplete";
        public const string PlayEnemyBleedAttackAnimation = "PlayEnemyBleedAttackAnimation";
        public const string PlayEnemyBleedAttackAnimationComplete = "PlayEnemyBleedAttackAnimationComplete";
        public const string PlayEnemyDevourAttackAnimation = "PlayEnemyDevourAttackAnimation";
        public const string PlayEnemyDevourAttackAnimationComplete = "PlayEnemyDevourAttackAnimationComplete";
        public const string PlayEnemyMassAttackAnimation = "PlayEnemyMassAttackAnimation";
        public const string PlayEnemyMassAttackAnimationComplete = "PlayEnemyMassAttackAnimationComplete";
    }
}