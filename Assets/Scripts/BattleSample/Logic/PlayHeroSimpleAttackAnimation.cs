﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayHeroSimpleAttackAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayHeroSimpleAttackAnimationComplete;

        public PlayHeroSimpleAttackAnimation()
        {
            _onPlayHeroSimpleAttackAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayHeroSimpleAttackAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayHeroSimpleAttackAnimationComplete,
                _onPlayHeroSimpleAttackAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayHeroSimpleAttackAnimationComplete,
                _onPlayHeroSimpleAttackAnimationComplete);
        }
    }
}