﻿using System;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class EnemySimpleAttack : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            const int warriorIndex = 0;
            var currentEnemy = MainModel.BattleSampleData.CurrentEnemy();
            var heroes = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData;
            var target = currentEnemy.taunted ? warriorIndex : currentEnemy.targets[0];
            var targetHero = heroes[target];
            var currentAbility = currentEnemy.CurrentAbility();
            var damageValue = currentAbility.power - targetHero.armor;
            damageValue = Math.Max(0, damageValue);
            targetHero.healthPoints -= damageValue;
            targetHero.abilityHurt = currentAbility;
            targetHero.hurtValue = damageValue;
            currentEnemy.active = false;
            currentAbility.coolDown = currentAbility.coolDownTotal;
            Complete();
        }
    }
}