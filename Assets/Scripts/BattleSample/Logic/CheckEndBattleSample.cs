﻿using System.Linq;
using BattleSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class CheckEndBattleSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            if (MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData.All(enemy => !enemy.alive))
                Observer.Emit(BattleSampleEvent.EnemyKilled);
            else if (MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData.All(hero => !hero.alive))
                Observer.Emit(BattleSampleEvent.PlayerKilled);
            Complete();
        }
    }
}