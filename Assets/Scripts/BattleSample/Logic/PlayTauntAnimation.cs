﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayTauntAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayTauntAnimationComplete;

        public PlayTauntAnimation()
        {
            _onPlayTauntAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayTauntAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayTauntAnimationComplete, _onPlayTauntAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayTauntAnimationComplete, _onPlayTauntAnimationComplete);
        }
    }
}