﻿using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class EnemyBleedAttack : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            const int warriorIndex = 0;
            var currentEnemy = MainModel.BattleSampleData.CurrentEnemy();
            var heroes = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData;
            var target = currentEnemy.taunted ? warriorIndex : currentEnemy.targets[0];
            var targetHero = heroes[target];
            var currentAbility = currentEnemy.CurrentAbility();
            targetHero.bleed = true;
            targetHero.bleedDuration = currentAbility.power;
            targetHero.bleedValue += currentAbility.powerOverTime;
            targetHero.abilityHurt = currentAbility;
            targetHero.hurtValue = currentAbility.powerOverTime;
            currentEnemy.active = false;
            currentAbility.coolDown = currentAbility.coolDownTotal;
            Complete();
        }
    }
}