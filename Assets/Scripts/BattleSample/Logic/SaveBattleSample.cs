﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Model;
using UnityEngine;

namespace BattleSample.Logic
{
    public class SaveBattleSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var loadData = JsonUtility.FromJson<PrimitiveDungeonCrawlerSampleData>(PlayerPrefs.GetString(JsonKeys.PrimitiveDungeonCrawlerSampleData));
            loadData.needLoad = MainModel.BattleSampleData.primitivePlayer.alive;
            loadData.primitivePlayer = MainModel.BattleSampleData.primitivePlayer;
            loadData.primitiveEncounterEnemy = MainModel.BattleSampleData.primitiveEncounterEnemy;
            loadData.primitiveEncounterEnemy.alive = MainModel.BattleSampleData.primitiveEncounterEnemy.alive;
            loadData.primitiveEncounterEnemy.killed = true;
            PlayerPrefs.SetString(JsonKeys.PrimitiveDungeonCrawlerSampleData, JsonUtility.ToJson(loadData));
            PlayerPrefs.Save();
            Complete();
        }
    }
}