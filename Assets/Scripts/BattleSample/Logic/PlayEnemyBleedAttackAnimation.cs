﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayEnemyBleedAttackAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayEnemyBleedAttackAnimationComplete;

        public PlayEnemyBleedAttackAnimation()
        {
            _onPlayEnemyBleedAttackAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayEnemyBleedAttackAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayEnemyBleedAttackAnimationComplete,
                _onPlayEnemyBleedAttackAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemyBleedAttackAnimationComplete,
                _onPlayEnemyBleedAttackAnimationComplete);
        }
    }
}