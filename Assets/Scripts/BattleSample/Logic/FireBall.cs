﻿using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class FireBall : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var currentHero = MainModel.BattleSampleData.CurrentHero();
            var target = MainModel.BattleSampleData.TargetEnemy();
            var currentAbility = currentHero.CurrentAbility();
            var damageValue = currentAbility.power;
            target.healthPoints -= damageValue;
            if (target.healthPoints == 0) target.alive = false;
            target.abilityHurt = currentAbility;
            target.hurtValue = damageValue;
            currentHero.active = false;
            currentAbility.coolDown = currentAbility.coolDownTotal;
            Complete();
        }
    }
}