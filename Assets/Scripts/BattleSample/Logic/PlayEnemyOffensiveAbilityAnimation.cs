﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayEnemyOffensiveAbilityAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayEnemyOffensiveAbilityAnimationComplete;

        public PlayEnemyOffensiveAbilityAnimation()
        {
            _onPlayEnemyOffensiveAbilityAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayEnemyOffensiveAbilityAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayEnemyOffensiveAbilityAnimationComplete,
                _onPlayEnemyOffensiveAbilityAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            Observer.RemoveListener(BattleSampleEvent.PlayEnemyOffensiveAbilityAnimationComplete,
                _onPlayEnemyOffensiveAbilityAnimationComplete);
        }
    }
}