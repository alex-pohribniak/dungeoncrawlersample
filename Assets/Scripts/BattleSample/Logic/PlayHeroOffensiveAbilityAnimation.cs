﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayHeroOffensiveAbilityAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayHeroOffensiveAbilityAnimationComplete;

        public PlayHeroOffensiveAbilityAnimation()
        {
            _onPlayHeroOffensiveAbilityAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayHeroOffensiveAbilityAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayHeroOffensiveAbilityAnimationComplete,
                _onPlayHeroOffensiveAbilityAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayHeroOffensiveAbilityAnimationComplete,
                _onPlayHeroOffensiveAbilityAnimationComplete);
        }
    }
}