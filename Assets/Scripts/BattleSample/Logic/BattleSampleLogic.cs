﻿using System.Linq;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using UnityEngine;

namespace BattleSample.Logic
{
    public class BattleSampleLogic : MonoBehaviour
    {
        private readonly TaskDelegate _onHeroActionComplete;
        private readonly TaskDelegate _onHeroesTurnComplete;
        private readonly TaskDelegate _onEnemiesTurnComplete;
        private readonly IntDelegate _onEnemySelect;
        private readonly IntDelegate _onHeroSelect;
        private readonly IntIntDelegate _onHeroAbilitySelect;
        private readonly SimpleDelegate _onEnemyKilled;
        private readonly SimpleDelegate _onPlayerKilled;
        
        public BaseTask startTask;
        public BaseTask selectAbility;
        public BaseTask heroesTurn;
        public BaseTask enemiesTurn;
        public BaseTask heroAction;
        public BaseTask endBattleSample;
        public BaseTask heroSimpleAttack;
        public BaseTask defence;
        public BaseTask taunt;
        public BaseTask armorBreak;
        public BaseTask fireBall;
        public BaseTask heal;


        public BattleSampleLogic()
        {
            _onHeroActionComplete = OnHeroActionComplete;
            _onHeroesTurnComplete = OnHeroesTurnComplete;
            _onEnemiesTurnComplete = OnEnemiesTurnComplete;
            _onHeroAbilitySelect = OnHeroAbilitySelect;
            _onHeroSelect = OnHeroSelect;
            _onEnemySelect = OnEnemySelect;
            _onEnemyKilled = OnEnemyKilled;
            _onPlayerKilled = OnPlayerKilled;
        }

        private void OnHeroActionComplete(BaseTask task)
        {
            var heroes = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData;
            if (heroes.Any(hero => hero.active)) return;
            
            heroAction.Complete();
        }

        private void OnHeroesTurnComplete(BaseTask task)
        {
            enemiesTurn.Execute();
        }

        private void OnEnemiesTurnComplete(BaseTask task)
        {
            heroesTurn.Execute();
        }

        private void OnHeroAbilitySelect(int heroIndex, int abilityIndex)
        {
            MainModel.BattleSampleData.currentHeroIndex = heroIndex;
            var currentHero = MainModel.BattleSampleData.CurrentHero();
            currentHero.selectedAbility = abilityIndex;
            selectAbility.Execute();

            if (currentHero.CurrentAbility().type == AbilityType.Defence) OnHeroSelect(heroIndex);
        }

        private void OnHeroSelect(int heroIndex)
        {
            if (MainModel.BattleSampleData.currentHeroIndex == -1) return;

            var currentHero = MainModel.BattleSampleData.CurrentHero();

            if (currentHero.active == false) return;

            if (currentHero.CurrentAbility().type == AbilityType.Heal)
            {
                currentHero.targetHero = heroIndex;
                heal.Execute();
            }
            else if (currentHero.CurrentAbility().type == AbilityType.Defence)
            {
                currentHero.targetHero = heroIndex;
                defence.Execute();
            }
        }

        private void OnEnemySelect(int enemyIndex)
        {
            if (MainModel.BattleSampleData.currentHeroIndex == -1) return;

            MainModel.BattleSampleData.currentEnemyIndex = enemyIndex;
            
            var currentHero = MainModel.BattleSampleData.CurrentHero();

            if (currentHero.active == false) return;

            if (currentHero.CurrentAbility().type == AbilityType.HeroSimpleAttack)
            {
                currentHero.targetEnemy = enemyIndex;
                heroSimpleAttack.Execute();
            }
            else if (currentHero.CurrentAbility().type == AbilityType.FireBall)
            {
                currentHero.targetEnemy = enemyIndex;
                fireBall.Execute();
            }
            else if (currentHero.CurrentAbility().type == AbilityType.ArmorBreak)
            {
                currentHero.targetEnemy = enemyIndex;
                armorBreak.Execute();
            }
            else if (currentHero.CurrentAbility().type == AbilityType.Taunt)
            {
                currentHero.targetEnemy = enemyIndex;
                taunt.Execute();
            }
        }

        private void OnPlayerKilled()
        {
            MainModel.BattleSampleData.primitivePlayer.alive = false;
            endBattleSample.Execute();
        }

        private void OnEnemyKilled()
        {
            MainModel.BattleSampleData.primitiveEncounterEnemy.alive = false;
            endBattleSample.Execute();
        }

        private void Start()
        {
            startTask.Execute();
            heroesTurn.Execute();
        }

        private void Awake()
        {
            Observer.AddListener(BattleSampleEvent.OnHeroAbilitySelect, _onHeroAbilitySelect);
            Observer.AddListener(BattleSampleEvent.OnHeroSelect, _onHeroSelect);
            Observer.AddListener(BattleSampleEvent.OnEnemySelect, _onEnemySelect);
            Observer.AddListener(BattleSampleEvent.PlayerKilled, _onPlayerKilled);
            Observer.AddListener(BattleSampleEvent.EnemyKilled, _onEnemyKilled);

            heroesTurn.onTaskComplete = _onHeroesTurnComplete;
            enemiesTurn.onTaskComplete = _onEnemiesTurnComplete;

            heroAction.onTaskComplete = _onHeroActionComplete;
            heroSimpleAttack.onTaskComplete = _onHeroActionComplete;
            defence.onTaskComplete = _onHeroActionComplete;
            taunt.onTaskComplete = _onHeroActionComplete;
            armorBreak.onTaskComplete = _onHeroActionComplete;
            fireBall.onTaskComplete = _onHeroActionComplete;
            heal.onTaskComplete = _onHeroActionComplete;
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.OnHeroAbilitySelect, _onHeroAbilitySelect);
            Observer.RemoveListener(BattleSampleEvent.OnHeroSelect, _onHeroSelect);
            Observer.RemoveListener(BattleSampleEvent.OnEnemySelect, _onEnemySelect);
            Observer.RemoveListener(BattleSampleEvent.PlayerKilled, _onPlayerKilled);
            Observer.RemoveListener(BattleSampleEvent.EnemyKilled, _onEnemyKilled);
        }
    }
}