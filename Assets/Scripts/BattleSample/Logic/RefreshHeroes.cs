﻿using BattleSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class RefreshHeroes : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var heroes = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData;
            foreach (var hero in heroes)
            {
                var abilities = hero.abilities;
                foreach (var ability in abilities)
                {
                    if (ability.coolDownTotal > 0 && ability.coolDown > 0) ability.coolDown--;
                    if (ability.type != AbilityType.Defence) continue;
                    if (!hero.defended) continue;
                    hero.defended = false;
                    hero.armor -= ability.power;
                }

                if (hero.bleed && hero.bleedDuration > 0)
                {
                    hero.bleedDuration--;
                    hero.healthPoints -= hero.bleedValue;
                }

                if (hero.bleedDuration == 0)
                {
                    hero.bleedValue = 0;
                    hero.bleed = false;
                }

                if (hero.healthPoints <= 0) hero.alive = false;
                if (hero.alive) hero.active = true;
            }

            MainModel.BattleSampleData.currentHeroIndex = -1;

            Complete();
        }
    }
}