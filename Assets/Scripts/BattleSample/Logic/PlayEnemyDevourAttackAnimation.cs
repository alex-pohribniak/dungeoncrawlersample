﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayEnemyDevourAttackAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayEnemyDevourAttackAnimationComplete;

        public PlayEnemyDevourAttackAnimation()
        {
            _onPlayEnemyDevourAttackAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayEnemyDevourAttackAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayEnemyDevourAttackAnimationComplete,
                _onPlayEnemyDevourAttackAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemyDevourAttackAnimationComplete,
                _onPlayEnemyDevourAttackAnimationComplete);
        }
    }
}