﻿using BattleSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class UpdateEnemiesView : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.UpdateEnemiesView);
            Complete();
        }
    }
}