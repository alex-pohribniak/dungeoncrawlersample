﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayArmorBreakAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayArmorBreakAnimationComplete;

        public PlayArmorBreakAnimation()
        {
            _onPlayArmorBreakAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayArmorBreakAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayArmorBreakAnimationComplete, _onPlayArmorBreakAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayArmorBreakAnimationComplete, _onPlayArmorBreakAnimationComplete);
        }
    }
}