﻿using Common.Tasks;
using UnityEngine.SceneManagement;

namespace BattleSample.Logic
{
    public class EndBattleSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            SceneManager.LoadScene("DungeonCrawlerSample");
            Complete();
        }
    }
}