﻿using BattleSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class EnemyAction : BaseTask
    {
        public BaseTask enemyBleedAttack;
        public BaseTask enemyDevourAttack;
        public BaseTask enemyMassAttack;
        public BaseTask enemySimpleAttack;

        protected override void OnExecute()
        {
            base.OnExecute();
            var enemies = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData;
            var index = ++MainModel.BattleSampleData.currentEnemyIndex;
            var enemy = enemies[index];
            if (!enemy.active) Complete();
            if (enemy.CurrentAbility().type == AbilityType.EnemySimpleAttack) enemySimpleAttack.Execute();
            if (enemy.CurrentAbility().type == AbilityType.EnemyBleedAttack) enemyBleedAttack.Execute();
            if (enemy.CurrentAbility().type == AbilityType.EnemyDevourAttack) enemyDevourAttack.Execute();
            if (enemy.CurrentAbility().type == AbilityType.EnemyMassAttack) enemyMassAttack.Execute();
        }

        protected override void Activate()
        {
            base.Activate();
            enemySimpleAttack.onTaskComplete = simpleAttack => { Complete(); };
            enemyBleedAttack.onTaskComplete = bleedAttack => { Complete(); };
            enemyDevourAttack.onTaskComplete = devourAttack => { Complete(); };
            enemyMassAttack.onTaskComplete = massAttack => { Complete(); };
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            enemySimpleAttack.onTaskComplete = null;
            enemyBleedAttack.onTaskComplete = null;
            enemyDevourAttack.onTaskComplete = null;
            enemyMassAttack.onTaskComplete = null;
        }
    }
}