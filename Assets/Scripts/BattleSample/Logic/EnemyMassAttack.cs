﻿using System;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class EnemyMassAttack : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var currentEnemy = MainModel.BattleSampleData.CurrentEnemy();
            var heroes = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData;
            var currentAbility = currentEnemy.CurrentAbility();
            for (var i = 0; i < heroes.Count; i++)
            {
                var target = heroes[i];
                var damageValue = currentAbility.power;
                damageValue = Math.Max(0, damageValue);
                target.healthPoints -= damageValue;
                target.abilityHurt = currentAbility;
                target.hurtValue = damageValue;
            }

            currentEnemy.active = false;
            currentAbility.coolDown = currentAbility.coolDownTotal;
            Complete();
        }
    }
}