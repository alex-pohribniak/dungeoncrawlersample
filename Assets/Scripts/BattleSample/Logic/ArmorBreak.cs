﻿using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class ArmorBreak : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var currentHero = MainModel.BattleSampleData.CurrentHero();
            var target = MainModel.BattleSampleData.TargetEnemy();
            var currentAbility = currentHero.CurrentAbility();
            var breakValue = currentAbility.power;
            target.armor -= breakValue;
            target.abilityHurt = currentAbility;
            target.hurtValue = breakValue;
            currentHero.active = false;
            currentAbility.coolDown = currentAbility.coolDownTotal;
            Complete();
        }
    }
}