﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayEnemySimpleAttackAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayEnemySimpleAttackAnimationComplete;

        public PlayEnemySimpleAttackAnimation()
        {
            _onPlayEnemySimpleAttackAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayEnemySimpleAttackAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayEnemySimpleAttackAnimationComplete,
                _onPlayEnemySimpleAttackAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemySimpleAttackAnimationComplete,
                _onPlayEnemySimpleAttackAnimationComplete);
        }
    }
}