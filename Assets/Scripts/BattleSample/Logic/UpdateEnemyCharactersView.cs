﻿using BattleSample.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class UpdateEnemyCharactersView : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.UpdateEnemyCharactersView);
            Complete();
        }
    }
}