﻿using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class Defence : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var currentHero = MainModel.BattleSampleData.CurrentHero();
            var targetHero = currentHero;
            var currentAbility = currentHero.CurrentAbility();
            targetHero.defended = true;
            targetHero.armor += currentAbility.power;
            currentHero.active = false;
            currentAbility.coolDown = currentAbility.coolDownTotal;
            Complete();
        }
    }
}