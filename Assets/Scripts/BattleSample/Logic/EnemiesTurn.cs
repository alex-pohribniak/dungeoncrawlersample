﻿using System.Linq;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class EnemiesTurn : BaseTask
    {
        private readonly TaskDelegate _onEnemyActionComplete;

        public BaseTask enemyAction;

        public EnemiesTurn()
        {
            _onEnemyActionComplete = OnEnemyActionComplete;
        }

        private void OnEnemyActionComplete(BaseTask task)
        {
            var enemies = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData;
            if (enemies.Any(enemy => enemy.active))
                enemyAction.Execute();
            else
                Complete();
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            enemyAction.Execute();
        }

        protected override void Activate()
        {
            base.Activate();
            enemyAction.onTaskComplete = _onEnemyActionComplete;
        }
    }
}