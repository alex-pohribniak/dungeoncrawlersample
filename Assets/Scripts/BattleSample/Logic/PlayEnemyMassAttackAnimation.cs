﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayEnemyMassAttackAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayEnemyMassAttackAnimationComplete;

        public PlayEnemyMassAttackAnimation()
        {
            _onPlayEnemyMassAttackAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayEnemyMassAttackAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayEnemyMassAttackAnimationComplete, _onPlayEnemyMassAttackAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemyMassAttackAnimationComplete,
                _onPlayEnemyMassAttackAnimationComplete);
        }
    }
}