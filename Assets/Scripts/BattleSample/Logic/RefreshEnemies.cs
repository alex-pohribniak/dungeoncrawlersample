﻿using System.Linq;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class RefreshEnemies : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var enemies = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData;
            foreach (var enemy in enemies)
            {
                var abilities = enemy.abilities;

                foreach (var ability in abilities.Where(ability => ability.coolDownTotal > 0 && ability.coolDown > 0))
                    ability.coolDown--;

                if (enemy.taunted && enemy.tauntDuration > 0) enemy.tauntDuration -= 1;
                if (enemy.tauntDuration == 0) enemy.taunted = false;
                if (enemy.healthPoints <= 0) enemy.alive = false;
                if (enemy.alive) enemy.active = true;
            }

            MainModel.BattleSampleData.currentEnemyIndex = -1;

            Complete();
        }
    }
}