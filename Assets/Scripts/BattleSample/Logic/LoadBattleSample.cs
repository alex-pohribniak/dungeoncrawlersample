﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using DungeonCrawlerSample.Model;
using UnityEngine;

namespace BattleSample.Logic
{
    public class LoadBattleSample : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var loadData =
                JsonUtility.FromJson<PrimitiveDungeonCrawlerSampleData>(
                    PlayerPrefs.GetString(JsonKeys.PrimitiveDungeonCrawlerSampleData));
            if (loadData == null)
            {
                Complete();
                return;
            }

            MainModel.BattleSampleData.primitivePlayer = loadData.primitivePlayer;
            MainModel.BattleSampleData.primitiveEncounterEnemy = loadData.primitiveEncounterEnemy;
            Complete();
        }
    }
}