﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayHealAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayHealAnimationComplete;

        public PlayHealAnimation()
        {
            _onPlayHealAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayHealAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayHealAnimationComplete, _onPlayHealAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            Observer.RemoveListener(BattleSampleEvent.PlayHealAnimationComplete, _onPlayHealAnimationComplete);
        }
    }
}