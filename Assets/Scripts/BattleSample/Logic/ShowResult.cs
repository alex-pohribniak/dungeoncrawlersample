﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class ShowResult : BaseTask
    {
        private readonly SimpleDelegate _onShowResultComplete;

        public ShowResult()
        {
            _onShowResultComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.ShowResult);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.ShowResultComplete, _onShowResultComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.ShowResultComplete, _onShowResultComplete);
        }
    }
}