﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayFireBallAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayFireBallAnimationComplete;

        public PlayFireBallAnimation()
        {
            _onPlayFireBallAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayFireBallAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayFireBallAnimationComplete, _onPlayFireBallAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayFireBallAnimationComplete, _onPlayFireBallAnimationComplete);
        }
    }
}