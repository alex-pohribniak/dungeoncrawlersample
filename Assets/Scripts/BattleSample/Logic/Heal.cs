﻿using System;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class Heal : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var currentHero = MainModel.BattleSampleData.CurrentHero();
            var targetHero = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData[currentHero.targetHero];
            var currentAbility = currentHero.CurrentAbility();
            targetHero.healthPoints += currentAbility.power;
            targetHero.healthPoints = Math.Min(targetHero.healthPoints, targetHero.maxHealthPoints);
            currentHero.active = false;
            currentAbility.coolDown = currentAbility.coolDownTotal;
            Complete();
        }
    }
}