﻿using System.Collections.Generic;
using System.Linq;
using BattleSample.Enums;
using Common.Singleton;
using Common.Tasks;
using UnityEngine;

namespace BattleSample.Logic
{
    public class ScheduleNextTurn : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var enemies = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData;
            foreach (var enemy in enemies)
            {
                var abilities = enemy.abilities;
                var possibleNextAbilities = abilities.Where(ability => ability.coolDown == 0).ToList();
                var nextAbility = possibleNextAbilities[Random.Range(0, possibleNextAbilities.Count)];
                for (var i = 0; i < abilities.Count; i++)
                    if (nextAbility.type == abilities[i].type)
                    {
                        enemy.nextAbility = i;
                        break;
                    }

                enemy.targets = new List<int>();
                var possibleTargets = new List<int>();
                var heroes = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData;
                for (var i = 0; i < heroes.Count; i++)
                    if (heroes[i].alive)
                        possibleTargets.Add(i);

                var randomPossibleTarget = possibleTargets[Random.Range(0, possibleTargets.Count)];

                if (nextAbility.type == AbilityType.EnemyMassAttack)
                    enemy.targets = possibleTargets;
                else
                    enemy.targets.Add(enemy.taunted ? 0 : randomPossibleTarget);
            }

            Complete();
        }
    }
}