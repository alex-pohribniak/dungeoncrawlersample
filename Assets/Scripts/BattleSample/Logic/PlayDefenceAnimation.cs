﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class PlayDefenceAnimation : BaseTask
    {
        private readonly SimpleDelegate _onPlayDefenceAnimationComplete;

        public PlayDefenceAnimation()
        {
            _onPlayDefenceAnimationComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(BattleSampleEvent.PlayDefenceAnimation);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(BattleSampleEvent.PlayDefenceAnimationComplete, _onPlayDefenceAnimationComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayDefenceAnimationComplete, _onPlayDefenceAnimationComplete);
        }
    }
}