﻿using System.Collections.Generic;
using Common.Singleton;
using Common.Tasks;

namespace BattleSample.Logic
{
    public class Taunt : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            var currentHero = MainModel.BattleSampleData.CurrentHero();
            var target = MainModel.BattleSampleData.TargetEnemy();
            var currentAbility = currentHero.CurrentAbility();
            var tauntValue = currentAbility.power;
            target.taunted = true;
            target.tauntDuration = tauntValue;
            target.abilityHurt = currentAbility;
            target.hurtValue = tauntValue;
            target.targets = new List<int> {0};
            currentHero.active = false;
            currentAbility.coolDown = currentAbility.coolDownTotal;
            Complete();
        }
    }
}