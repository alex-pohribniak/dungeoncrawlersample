﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class DefenceAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayDefenceAnimation;

        public GameObject defence;
        public AnimationViewLinks viewLinks;

        public DefenceAnimation()
        {
            _onPlayDefenceAnimation = OnPlayDefenceAnimation;
        }

        private void OnPlayDefenceAnimation()
        {
            duration = 0.5f;
            ClearAnimation();
            CreateSequence();
            var defenceClone = Instantiate(defence, defence.transform.parent);
            defenceClone.SetActive(true);
            var tweenScale = defenceClone.transform.DOPunchScale(new Vector3(0.2f, 0.2f, 0.2f), duration, 0, 0f);
            Join(tweenScale);
            var targetHeroPosition = viewLinks.DefineHero().avatarImage.transform.position;
            var tweenMove = defenceClone.transform.DOMove(targetHeroPosition, duration);
            Join(tweenMove);
            OnComplete(() =>
            {
                Destroy(defenceClone);
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayDefenceAnimationComplete);
            });
            Play();
        }


        protected override void Awake()
        {
            base.Awake();
            defence.SetActive(false);
            Observer.AddListener(BattleSampleEvent.PlayDefenceAnimation, _onPlayDefenceAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayDefenceAnimation, _onPlayDefenceAnimation);
        }
    }
}