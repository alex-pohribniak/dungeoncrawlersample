﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class ArmorBreakAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayArmorBreakAnimation;
        
        public GameObject armorBreak;
        public AnimationViewLinks viewLinks;

        public ArmorBreakAnimation()
        {
            _onPlayArmorBreakAnimation = OnPlayArmorBreakAnimation;
        }

        private void OnPlayArmorBreakAnimation()
        {
            duration = 1.0f;
            ClearAnimation();
            CreateSequence();
            var armorBreakClone = Instantiate(armorBreak, armorBreak.transform.parent);
            armorBreakClone.SetActive(true);
            var targetEnemy = viewLinks.DefineEnemy();
            var targetPosition = targetEnemy.transform.position;
            armorBreakClone.transform.position = targetPosition;
            var tweenScale = armorBreakClone.transform.DOScale(1.5f, duration);
            var tweenShake = targetEnemy.transform.DOShakePosition(duration, 10f, 10, 10f);
            Join(tweenScale);
            Join(tweenShake);
            OnComplete(() =>
            {
                Destroy(armorBreakClone);
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayArmorBreakAnimationComplete);
            });
            Play();
            armorBreakClone.GetComponent<ParticleSystem>().Play();
        }
        
        protected override void Awake()
        {
            base.Awake();
            armorBreak.SetActive(false);
            Observer.AddListener(BattleSampleEvent.PlayArmorBreakAnimation, _onPlayArmorBreakAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull())
            {
                return;
            }
            Observer.RemoveListener(BattleSampleEvent.PlayArmorBreakAnimation, _onPlayArmorBreakAnimation);
        }
    }
}
