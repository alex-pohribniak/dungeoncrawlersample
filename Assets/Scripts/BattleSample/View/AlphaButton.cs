﻿using UnityEngine;
using UnityEngine.UI;

namespace BattleSample.View
{
    public class AlphaButton : MonoBehaviour
    {
        private const float AlphaThreshold = .1f;

        private void Awake()
        {
            GetComponent<Image>().alphaHitTestMinimumThreshold = AlphaThreshold;
        }
    }
}