﻿using System;
using System.Collections.Generic;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using UnityEngine;
using UnityEngine.UI;

namespace BattleSample.View
{
    public class HeroView : MonoBehaviour
    {
        private List<HeroAbilityView> _abilityViews;

        public HeroAbilityView ability0;
        public HeroAbilityView ability1;
        public HeroAbilityView ability2;
        public Slider healthPointsBar;
        public Slider armorPointsBar;
        public Text healthPointsValue;
        public Text armorPointsValue;
        public Button avatarButton;
        public Image avatarImage;
        public Image frame;
        public Image target;
        public Image dead;
        public Image classAbilityImage;
        public Text classAbilityCoolDown;
        public Image abilitiesPanel;
        public int index;
        public Image defended;
        public Image bleed;

        public void UpdateView()
        {
            if (avatarImage.sprite == null || classAbilityImage.sprite == null)
            {
                LoadClassImages();
            }
            
            var hero = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData[index];
            if (!hero.active)
            {
                frame.gameObject.SetActive(false);
                avatarButton.interactable = false;
            }
            else
            {
                frame.gameObject.SetActive(MainModel.BattleSampleData.currentHeroIndex == index);
                avatarButton.interactable = true;
            }

            bleed.gameObject.SetActive(hero.bleed);
            
            defended.gameObject.SetActive(hero.defended);

            target.gameObject.SetActive(hero.enemyTarget);

            dead.gameObject.SetActive(!hero.alive);

            healthPointsBar.maxValue = hero.maxHealthPoints;
            healthPointsBar.value = hero.healthPoints;
            healthPointsValue.text = $"{healthPointsBar.value}/{healthPointsBar.maxValue}";
            armorPointsBar.maxValue = hero.maxArmor;
            armorPointsBar.value = hero.armor;
            armorPointsValue.text = $"{armorPointsBar.value}/{armorPointsBar.maxValue}";

            classAbilityCoolDown.text = hero.abilities[hero.abilities.Count-1].coolDown.ToString();

        }

        private void UpdateAbilities()
        {
            var hero = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData[index];
            for (var i = 0; i < _abilityViews.Count; i++)
            {
                _abilityViews[i].UpdateView(hero.abilities[i].coolDown);
            }
        }

        public void UpdateAbilitiesFrames(int abilityIndex)
        {
            for (var i = 0; i < _abilityViews.Count; i++)
            {
                _abilityViews[i].frame.gameObject.SetActive(abilityIndex == i);
            }
        }

        public void OnAbilitySelect(int abilityIndex)
        {
            UpdateAbilitiesFrames(abilityIndex);
            Observer.Emit(BattleSampleEvent.OnHeroAbilitySelect, index, abilityIndex);
        }

        public void OnHeroSelect()
        {
            UpdateAbilities();
            UpdateAbilitiesFrames(0);
            Observer.Emit(BattleSampleEvent.OnHeroSelect, index);
        }

        private void LoadClassImages()
        {
            var heroType = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData[index].type;
            switch (heroType)
            {
                case HeroType.Warrior:
                    avatarImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Heroes/{heroType}");
                    classAbilityImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Heroes/{heroType}Ability2");
                    break;
                case HeroType.Rogue:
                    avatarImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Heroes/{heroType}");
                    classAbilityImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Heroes/{heroType}Ability2");
                    break;
                case HeroType.Wizard:
                    avatarImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Heroes/{heroType}");
                    classAbilityImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Heroes/{heroType}Ability2");
                    break;
                case HeroType.Cleric:
                    avatarImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Heroes/{heroType}");
                    classAbilityImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Heroes/{heroType}Ability2");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Awake()
        {
            _abilityViews = new List<HeroAbilityView> {ability0, ability1, ability2};
        }
    }
}