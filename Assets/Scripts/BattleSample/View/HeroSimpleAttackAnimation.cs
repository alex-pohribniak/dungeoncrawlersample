﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class HeroSimpleAttackAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayHeroSimpleAttackAnimation;
        public ParticleSystem bloodSplash;
        public GameObject heroSimpleAttack;

        public Camera mainCamera;
        public AnimationViewLinks viewLinks;

        public HeroSimpleAttackAnimation()
        {
            _onPlayHeroSimpleAttackAnimation = OnPlayHeroSimpleAttackAnimation;
        }

        private void OnPlayHeroSimpleAttackAnimation()
        {
            duration = 0.3f;
            var halfDuration = duration / 2;
            ClearAnimation();
            CreateSequence();
            var heroAttack = Instantiate(heroSimpleAttack, heroSimpleAttack.transform.parent);
            heroAttack.SetActive(true);
            var heroAttackPosition = heroAttack.transform.position;
            var targetEnemy = viewLinks.DefineEnemy();
            var targetPosition = mainCamera.ScreenToWorldPoint(targetEnemy.transform.position);
            targetPosition -= heroAttackPosition;
            var oXVector = new Vector3(1f, 0f, 0f);
            var angle = Vector3.Angle(oXVector, targetPosition) * -1f;
            var attackVector = new Vector3(0f, 0f, angle);
            var tweenRotate = heroAttack.transform.DORotate(attackVector, duration);
            var tweenShake = targetEnemy.transform.DOShakePosition(halfDuration, 10f, 10, 10f);
            tweenShake.SetDelay(halfDuration);
            Join(tweenRotate);
            Join(tweenShake);
            SetEase(Ease.InQuad);
            OnComplete(() =>
            {
                Destroy(heroAttack);
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayHeroSimpleAttackAnimationComplete);
            });
            Play();
            MakeBloodSplash();
        }

        private void MakeBloodSplash()
        {
            var vectorYShift = new Vector3(0f, 100f, 0f);
            bloodSplash.transform.position = viewLinks.DefineEnemy().transform.position + vectorYShift;
            bloodSplash.Play();
        }

        protected override void Awake()
        {
            base.Awake();
            heroSimpleAttack.SetActive(false);
            Observer.AddListener(BattleSampleEvent.PlayHeroSimpleAttackAnimation, _onPlayHeroSimpleAttackAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayHeroSimpleAttackAnimation, _onPlayHeroSimpleAttackAnimation);
        }
    }
}