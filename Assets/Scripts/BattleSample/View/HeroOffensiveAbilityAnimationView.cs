﻿using System;
using System.Collections.Generic;
using BattleSample.Config;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace BattleSample.View
{
    public class HeroOffensiveAbilityAnimationView : MonoBehaviour
    {
        public Text enemyDamageTakenView0;
        public Text enemyDamageTakenView1;
        public Text enemyDamageTakenView2;

        private readonly SimpleDelegate _onPlayHeroOffensiveAbilityAnimation;
        private List<Text> _enemyDamageTakenViews;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public HeroOffensiveAbilityAnimationView()
        {
            _onPlayHeroOffensiveAbilityAnimation = OnPlayHeroOffensiveAbilityAnimation;
        }

        protected void Awake()
        {
            _enemyDamageTakenViews = new List<Text>
            {
                enemyDamageTakenView0, enemyDamageTakenView1, enemyDamageTakenView2
            };

            HideViews();

            Observer.AddListener(BattleSampleEvent.PlayHeroOffensiveAbilityAnimation,
                _onPlayHeroOffensiveAbilityAnimation);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayHeroOffensiveAbilityAnimation,
                _onPlayHeroOffensiveAbilityAnimation);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnPlayHeroOffensiveAbilityAnimation()
        {
            ClearTween();
            _tweenAnimation = DOTween.Sequence();
            var customView = SetupView();
            var color = customView.color;
            var tweenX = customView.transform.DOMoveX(AbilityAnimationConfig.MinusShiftX,
                AbilityAnimationConfig.Duration);
            var tweenColor = customView.DOColor(new Color(color.r, color.g, color.b, 0),
                AbilityAnimationConfig.Duration);
            _tweenAnimation.Join(tweenX);
            _tweenAnimation.Join(tweenColor);
            _tweenAnimation.OnComplete(() =>
            {
                Destroy(customView);
                Observer.Emit(BattleSampleEvent.PlayHeroOffensiveAbilityAnimationComplete);
            });
            _tweenAnimation.Play();
        }

        private Text SetupView()
        {
            var targetEnemy = MainModel.BattleSampleData.currentEnemyIndex;
            var customView = Instantiate(_enemyDamageTakenViews[targetEnemy],
                _enemyDamageTakenViews[targetEnemy].transform.parent);
            var hurtValue = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData[targetEnemy]
                .hurtValue;
            var abilityHurt =
                MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData[targetEnemy].abilityHurt;
            customView.gameObject.SetActive(hurtValue > 0);
            customView.text = hurtValue.ToString();
            switch (abilityHurt.type)
            {
                case AbilityType.HeroSimpleAttack:
                    customView.color = Color.red;
                    break;
                case AbilityType.Taunt:
                    customView.text = "Taunted!";
                    customView.color = new Color(1f, .5f, 0f, 1f);
                    break;
                case AbilityType.ArmorBreak:
                    customView.color = Color.grey;
                    break;
                case AbilityType.FireBall:
                    customView.color = Color.yellow;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return customView;
        }

        private void HideViews()
        {
            foreach (var view in _enemyDamageTakenViews) view.gameObject.SetActive(false);
        }
    }
}