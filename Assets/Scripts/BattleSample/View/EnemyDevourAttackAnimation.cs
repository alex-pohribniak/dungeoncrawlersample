﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class EnemyDevourAttackAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayEnemyDevourAttackAnimation;

        public ParticleSystem devourDistort;
        public AnimationViewLinks viewLinks;
        
        public EnemyDevourAttackAnimation()
        {
            _onPlayEnemyDevourAttackAnimation = OnPlayEnemyDevourAttackAnimation;
        }

        private void OnPlayEnemyDevourAttackAnimation()
        {
            duration = 1.0f;
            ClearAnimation();
            CreateSequence();
            var enemy = viewLinks.DefineEnemy();
            var tweenShake = enemy.transform.DOShakePosition(duration, 10f, 10, 10f);
            var tweenMove = devourDistort.transform.DOMove(enemy.transform.position, 0f);
            Join(tweenShake);
            Join(tweenMove);
            OnComplete(() =>
            {
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayEnemyDevourAttackAnimationComplete);
            });
            Play();
            devourDistort.Play();
        }

        protected override void Awake()
        {
            base.Awake();
            Observer.AddListener(BattleSampleEvent.PlayEnemyDevourAttackAnimation, _onPlayEnemyDevourAttackAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemyDevourAttackAnimation, _onPlayEnemyDevourAttackAnimation);
        }
    }
}