﻿using System;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class EnemySimpleAttackAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayEnemySimpleAttackAnimation;
        public GameObject archmageSimpleAttack;
        public ParticleSystem bloodSplash;
        public GameObject demonSimpleAttack;
        public AnimationViewLinks viewLinks;

        public GameObject werewolfSimpleAttack;

        public EnemySimpleAttackAnimation()
        {
            _onPlayEnemySimpleAttackAnimation = OnPlayEnemySimpleAttackAnimation;
        }

        private void OnPlayEnemySimpleAttackAnimation()
        {
            var enemyType = MainModel.BattleSampleData.primitiveEncounterEnemy
                .battleSampleEnemiesData[MainModel.BattleSampleData.currentEnemyIndex].type;
            switch (enemyType)
            {
                case EnemyType.Werewolf:
                    DoWerewolfAttack();
                    break;
                case EnemyType.Demon:
                    DoDemonAttack();
                    break;
                case EnemyType.Archmage:
                    DoArchmageAttack();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void DoWerewolfAttack()
        {
            duration = 0.5f;
            ClearAnimation();
            CreateSequence();
            var enemy = viewLinks.DefineEnemy();
            var targetPosition = new Vector3(350f, -150f, 0f);
            var enemyAttack = Instantiate(werewolfSimpleAttack, werewolfSimpleAttack.transform.parent);
            enemyAttack.SetActive(true);
            var tweenShake = enemy.transform.DOShakePosition(duration, 10f, 10, 10f);
            var tweenMove = enemyAttack.transform.DOMove(targetPosition, duration);
            Join(tweenShake);
            Join(tweenMove);
            SetEase(Ease.InOutCubic);
            OnComplete(() =>
            {
                Destroy(enemyAttack);
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayEnemySimpleAttackAnimationComplete);
            });
            Play();
            MakeBloodSplash();
        }

        private void DoDemonAttack()
        {
            duration = 0.5f;
            var halfDuration = duration / 2f;
            ClearAnimation();
            CreateSequence();
            var enemy = viewLinks.DefineEnemy();
            var tweenShake = enemy.transform.DOShakePosition(duration, 10f, 10, 10f);
            var targetPosition1 = new Vector3(350f, -150f, 0f);
            var targetPosition2 = new Vector3(-350f, -150f, 0f);
            var parent = demonSimpleAttack.transform.parent;
            var enemyAttack1 = Instantiate(demonSimpleAttack, parent);
            enemyAttack1.gameObject.SetActive(true);
            var firstPosition = enemyAttack1.transform.position;
            var secondPosition = new Vector3(firstPosition.x * -1, firstPosition.y, firstPosition.z);
            var enemyAttack2 = Instantiate(demonSimpleAttack, secondPosition,
                Quaternion.AngleAxis(180f, Vector3.forward), parent);
            enemyAttack2.gameObject.SetActive(true);
            var tweenMoveOne = enemyAttack1.transform.DOMove(targetPosition1, halfDuration);
            var tweenMoveTwo = enemyAttack2.transform.DOMove(targetPosition2, halfDuration);
            Join(tweenShake);
            Join(tweenMoveOne);
            Append(tweenMoveTwo);
            OnComplete(() =>
            {
                Destroy(enemyAttack1);
                Destroy(enemyAttack2);
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayEnemySimpleAttackAnimationComplete);
            });
            Play();
            MakeBloodSplash();
        }

        private void DoArchmageAttack()
        {
            duration = 2.5f;
            var halfDuration = duration / 2f;
            ClearAnimation();
            CreateSequence();
            var enemy = viewLinks.DefineEnemy();
            var tweenShake = enemy.transform.DOShakePosition(halfDuration, 10f, 10, 10f);
            Join(tweenShake);
            var enemyAttack = Instantiate(archmageSimpleAttack, archmageSimpleAttack.transform.parent);
            enemyAttack.transform.position = enemy.transform.position + new Vector3(0f, 100f, 0f);
            enemyAttack.SetActive(true);
            var tweenRotate = enemyAttack.transform.DORotate(new Vector3(0f, 0f, 180f), duration);
            Join(tweenRotate);
            OnComplete(() =>
            {
                Destroy(enemyAttack);
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayEnemySimpleAttackAnimationComplete);
            });
            Play();
            enemyAttack.GetComponent<ParticleSystem>().Play();
        }

        private void MakeBloodSplash()
        {
            bloodSplash.transform.position = viewLinks.DefineEnemyTargetHero().avatarImage.transform.position;
            bloodSplash.Play();
        }

        protected override void Awake()
        {
            base.Awake();
            werewolfSimpleAttack.SetActive(false);
            demonSimpleAttack.SetActive(false);
            archmageSimpleAttack.SetActive(false);
            Observer.AddListener(BattleSampleEvent.PlayEnemySimpleAttackAnimation, _onPlayEnemySimpleAttackAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemySimpleAttackAnimation, _onPlayEnemySimpleAttackAnimation);
        }
    }
}