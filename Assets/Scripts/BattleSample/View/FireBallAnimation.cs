﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class FireBallAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayFireBallAnimation;
        
        public GameObject fireBall;
        public AnimationViewLinks viewLinks;        

        public FireBallAnimation()
        {
            _onPlayFireBallAnimation = OnPlayFireBallAnimation;
        }

        private void OnPlayFireBallAnimation()
        {
            duration = 1.0f;
            ClearAnimation();
            CreateSequence();
            var fireBallClone = Instantiate(fireBall, fireBall.transform.parent);
            fireBallClone.SetActive(true);
            fireBallClone.GetComponent<ParticleSystem>().Play();
            var fireBallPosition = fireBallClone.transform.position;
            var targetEnemy = viewLinks.DefineEnemy();
            var targetPosition = targetEnemy.transform.position;
            var oXVector = new Vector3(1f, 0f, 0f);
            var angle = Vector3.Angle(oXVector, targetPosition - fireBallPosition) - 90f;
            var fireBallVector = new Vector3(0f, 0f, angle);
            var tweenRotate = fireBallClone.transform.DORotate(fireBallVector, 0f);
            var tweenMoveX = fireBallClone.transform.DOMoveX(targetPosition.x, duration);
            tweenMoveX.SetEase(Ease.InCubic);
            var tweenMoveY = fireBallClone.transform.DOMoveY(targetPosition.y, duration);
            tweenMoveY.SetEase(Ease.OutCubic);
            Join(tweenRotate);
            Join(tweenMoveX);
            Join(tweenMoveY);
            OnComplete(() =>
            {
                Destroy(fireBallClone);
                ClearAnimation();
                MakeExplosion();
            });
            Play();
        }

        private void MakeExplosion()
        {
            duration = 1.0f;
            ClearAnimation();
            CreateSequence();
            var targetEnemy = viewLinks.DefineEnemy();
            var targetPosition = targetEnemy.transform.position;
            var tweenShake = targetEnemy.transform.DOShakePosition(duration, 10f, 10, 10f);
            Join(tweenShake);
            spriteRenderer.gameObject.SetActive(true);
            LoadSprites("BattleSample/Sprites/AbilityVisual/Explosion");
            spriteRenderer.transform.position = targetPosition;
            var tweenExplosion = DoSpriteAnimation(duration);
            tweenExplosion.OnUpdate(UpdateFrame);
            Join(tweenExplosion);
            OnComplete(() =>
            {
                ClearAnimation();
                spriteRenderer.gameObject.SetActive(false);
                Observer.Emit(BattleSampleEvent.PlayFireBallAnimationComplete);
            });
            Play();
        }

        protected override void Awake()
        {
            base.Awake();
            fireBall.SetActive(false);
            spriteRenderer.gameObject.SetActive(false);
            Observer.AddListener(BattleSampleEvent.PlayFireBallAnimation, _onPlayFireBallAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayFireBallAnimation, _onPlayFireBallAnimation);
        }
    }
}