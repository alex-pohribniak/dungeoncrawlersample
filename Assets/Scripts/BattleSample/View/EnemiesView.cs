﻿using System.Collections.Generic;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using UnityEngine;

namespace BattleSample.View
{
    public class EnemiesView : MonoBehaviour
    {
        private readonly SimpleDelegate _onUpdateEnemiesView;

        private List<EnemyView> _enemyViews;

        public EnemyView enemyView0;
        public EnemyView enemyView1;
        public EnemyView enemyView2;

        public EnemiesView()
        {
            _onUpdateEnemiesView = OnUpdateEnemiesView;
        }

        private void OnUpdateEnemiesView()
        {
            foreach (var enemyView in _enemyViews) enemyView.UpdateView();
        }

        private void Awake()
        {
            _enemyViews = new List<EnemyView> {enemyView0, enemyView1, enemyView2};
            Observer.AddListener(BattleSampleEvent.UpdateEnemiesView, _onUpdateEnemiesView);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.UpdateEnemiesView, _onUpdateEnemiesView);
        }
    }
}