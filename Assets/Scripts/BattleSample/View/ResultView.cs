﻿using System.Linq;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace BattleSample.View
{
    public class ResultView : MonoBehaviour
    {
        public Image result;

        private readonly SimpleDelegate _onShowResult;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public ResultView()
        {
            _onShowResult = OnShowResult;
        }

        protected void Awake()
        {
            result.gameObject.SetActive(false);
            Observer.AddListener(BattleSampleEvent.ShowResult, _onShowResult);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.ShowResult, _onShowResult);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnShowResult()
        {
            ClearTween();
            _tweenDuration = 3f;
            _tweenAnimation = DOTween.Sequence();
            result.gameObject.SetActive(true);
            result.sprite = DefineResultSprite();
            var tweenFade = result.DOFade(1f, _tweenDuration);
            _tweenAnimation.Join(tweenFade);
            _tweenAnimation.OnComplete(() => { Observer.Emit(BattleSampleEvent.ShowResultComplete); });
            _tweenAnimation.Play();
        }

        private static Sprite DefineResultSprite()
        {
            if (MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData.All(enemy => !enemy.alive))
                return Resources.Load<Sprite>("Common/Sprites/Win");
            return MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData.All(hero => !hero.alive)
                ? Resources.Load<Sprite>("Common/Sprites/GameOver")
                : default;
        }
    }
}