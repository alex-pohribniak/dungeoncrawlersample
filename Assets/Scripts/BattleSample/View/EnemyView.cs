﻿using System;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using UnityEngine;
using UnityEngine.UI;

namespace BattleSample.View
{
    public class EnemyView : MonoBehaviour
    {
        public Slider armorPointsBar;
        public Text armorPointsValue;
        public Button avatarButton;
        public Image avatarImage;
        public Image dead;
        public Slider healthPointsBar;
        public Text healthPointsValue;
        public int index;
        public Image nextAbility;
        public Image targetHero;
        public Text tauntDuration;
        public Image taunted;

        public void UpdateView()
        {
            if (!IsEnemyExist()) return;

            LoadEnemyAvatars();

            var enemy = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData[index];
            avatarButton.interactable = enemy.alive;

            dead.gameObject.SetActive(!enemy.alive);

            healthPointsBar.maxValue = enemy.maxHealthPoints;
            healthPointsBar.value = enemy.healthPoints;
            healthPointsValue.text = $"{healthPointsBar.value}/{healthPointsBar.maxValue}";
            armorPointsBar.maxValue = enemy.maxArmor;
            armorPointsBar.value = enemy.armor;
            armorPointsValue.text = $"{armorPointsBar.value}/{armorPointsBar.maxValue}";

            if (enemy.taunted)
            {
                taunted.gameObject.SetActive(true);
                tauntDuration.gameObject.SetActive(true);
                tauntDuration.text = enemy.tauntDuration.ToString();
            }
            else
            {
                taunted.gameObject.SetActive(false);
                tauntDuration.gameObject.SetActive(false);
            }

            UpdateNextAbility();
            UpdateTargetHero();
        }

        private bool IsEnemyExist()
        {
            try
            {
                var enemy = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData[index];
            }
            catch (ArgumentOutOfRangeException)
            {
                gameObject.SetActive(false);
                return false;
            }

            return true;
        }

        private void UpdateNextAbility()
        {
            var enemy = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData[index];
            nextAbility.sprite =
                Resources.Load<Sprite>($"BattleSample/Sprites/Enemies/{enemy.type}Ability{enemy.nextAbility}");
        }

        private void UpdateTargetHero()
        {
            var enemy = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData[index];
            var targetHeroType = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData[enemy.targets[0]].type;
            targetHero.sprite = Resources.Load<Sprite>(enemy.targets.Count == 1
                ? $"BattleSample/Sprites/Heroes/{targetHeroType}"
                : "BattleSample/Sprites/Heroes/Group");
        }

        public void OnEnemySelect()
        {
            Observer.Emit(BattleSampleEvent.OnEnemySelect, index);
        }

        private void LoadEnemyAvatars()
        {
            if (avatarImage.sprite != null) return;
            var enemyType = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData[index].type;
            switch (enemyType)
            {
                case EnemyType.Werewolf:
                    avatarImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Enemies/{enemyType}Avatar");
                    break;
                case EnemyType.Archmage:
                    avatarImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Enemies/{enemyType}Avatar");
                    break;
                case EnemyType.Demon:
                    avatarImage.sprite = Resources.Load<Sprite>($"BattleSample/Sprites/Enemies/{enemyType}Avatar");
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}