﻿using UnityEngine;
using UnityEngine.UI;

namespace BattleSample.View
{
    public class HeroAbilityView : MonoBehaviour
    {
        public Button abilityButton;
        public Text coolDown;
        public Image frame;

        public void UpdateView(int coolDownValue)
        {
            frame.gameObject.SetActive(false);
            if (coolDownValue > 0)
            {
                abilityButton.interactable = false;
                coolDown.gameObject.SetActive(true);
                coolDown.text = coolDownValue.ToString();
                return;
            }

            coolDown.gameObject.SetActive(false);
            abilityButton.interactable = true;
        }
    }
}