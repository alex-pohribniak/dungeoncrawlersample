﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class EnemyBleedAttackAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayEnemyBleedAttackAnimation;
        public ParticleSystem bloodLines;

        public ParticleSystem bloodSplash;
        public AnimationViewLinks viewLinks;

        public EnemyBleedAttackAnimation()
        {
            _onPlayEnemyBleedAttackAnimation = OnPlayEnemyBleedAttackAnimation;
        }

        private void OnPlayEnemyBleedAttackAnimation()
        {
            duration = 0.5f;
            ClearAnimation();
            CreateSequence();
            var enemy = viewLinks.DefineEnemy();
            var tweenShake = enemy.transform.DOShakePosition(duration, 10f, 10, 10f);
            Join(tweenShake);
            OnComplete(() =>
            {
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayEnemyBleedAttackAnimationComplete);
            });
            Play();
            bloodLines.Play();
            MakeBloodSplash();
        }

        private void MakeBloodSplash()
        {
            bloodSplash.transform.position = viewLinks.DefineEnemyTargetHero().avatarImage.transform.position;
            bloodSplash.Play();
        }

        protected override void Awake()
        {
            base.Awake();
            Observer.AddListener(BattleSampleEvent.PlayEnemyBleedAttackAnimation, _onPlayEnemyBleedAttackAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemyBleedAttackAnimation, _onPlayEnemyBleedAttackAnimation);
        }
    }
}