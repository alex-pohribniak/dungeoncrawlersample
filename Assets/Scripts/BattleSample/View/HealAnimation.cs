﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class HealAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayHealAnimation;

        public GameObject heal;
        public AnimationViewLinks viewLinks;

        public HealAnimation()
        {
            _onPlayHealAnimation = OnPlayHealAnimation;
        }

        private void OnPlayHealAnimation()
        {
            duration = 1.0f;
            ClearAnimation();
            CreateSequence();
            var healClone = Instantiate(heal, heal.transform.parent);
            healClone.SetActive(true);
            var targetHeroPosition = viewLinks.DefineHero().avatarImage.transform.position;
            var tweenMove = healClone.transform.DOMove(targetHeroPosition, duration);
            Join(tweenMove);
            SetEase(Ease.InOutQuad);
            OnComplete(() =>
            {
                Destroy(healClone);
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayHealAnimationComplete);
            });
            Play();
            healClone.GetComponent<ParticleSystem>().Play();
        }

        protected override void Awake()
        {
            base.Awake();
            heal.SetActive(false);
            Observer.AddListener(BattleSampleEvent.PlayHealAnimation, _onPlayHealAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayHealAnimation, _onPlayHealAnimation);
        }
    }
}