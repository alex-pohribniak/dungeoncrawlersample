﻿using System.Collections.Generic;
using System.Linq;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using UnityEngine;

namespace BattleSample.View
{
    public class HeroesView : MonoBehaviour
    {
        private readonly SimpleDelegate _onUpdateHeroesView;
        private List<HeroView> _heroViews;

        public HeroView heroView0;
        public HeroView heroView1;
        public HeroView heroView2;
        public HeroView heroView3;

        public HeroesView()
        {
            _onUpdateHeroesView = OnUpdateHeroesView;
        }

        private void OnUpdateHeroesView()
        {
            foreach (var heroView in _heroViews) heroView.UpdateView();

            if (_heroViews.Any(heroView => heroView.frame.IsActive())) return;

            var nextActive = 0;
            var heroes = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData;
            foreach (var heroView in _heroViews)
                if (nextActive == heroView.index && heroes[nextActive].active)
                {
                    OnHeroSelect(nextActive);
                    heroView.OnHeroSelect();
                    heroView.OnAbilitySelect(0);
                }
                else
                {
                    nextActive++;
                }
        }

        public void OnHeroSelect(int heroIndex)
        {
            foreach (var heroView in _heroViews)
            {
                heroView.frame.gameObject.SetActive(heroView.index == heroIndex);
                heroView.abilitiesPanel.gameObject.SetActive(heroView.index == heroIndex);
                if (heroView.index == heroIndex)
                {
                    heroView.OnAbilitySelect(0);
                }
            }
        }

        public void HideAbilitiesPanel()
        {
            foreach (var heroView in _heroViews) heroView.abilitiesPanel.gameObject.SetActive(false);
        }

        private void Awake()
        {
            _heroViews = new List<HeroView> {heroView0, heroView1, heroView2, heroView3};

            Observer.AddListener(BattleSampleEvent.UpdateHeroesView, _onUpdateHeroesView);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;

            Observer.RemoveListener(BattleSampleEvent.UpdateHeroesView, _onUpdateHeroesView);
        }
    }
}