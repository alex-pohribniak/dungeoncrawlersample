﻿using System;
using System.Collections.Generic;
using BattleSample.Config;
using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace BattleSample.View
{
    public class EnemyOffensiveAbilityAnimationView : MonoBehaviour
    {
        public Text heroDamageTakenView0;
        public Text heroDamageTakenView1;
        public Text heroDamageTakenView2;
        public Text heroDamageTakenView3;
        private readonly SimpleDelegate _onPlayEnemyOffensiveAbilityAnimation;
        private List<Text> _heroDamageTakenViews;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public EnemyOffensiveAbilityAnimationView()
        {
            _onPlayEnemyOffensiveAbilityAnimation = OnPlayEnemyOffensiveAbilityAnimation;
        }

        protected void Awake()
        {
            _heroDamageTakenViews = new List<Text>
            {
                heroDamageTakenView0, heroDamageTakenView1, heroDamageTakenView2, heroDamageTakenView3
            };

            HideViews();

            Observer.AddListener(BattleSampleEvent.PlayEnemyOffensiveAbilityAnimation,
                _onPlayEnemyOffensiveAbilityAnimation);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemyOffensiveAbilityAnimation,
                _onPlayEnemyOffensiveAbilityAnimation);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnPlayEnemyOffensiveAbilityAnimation()
        {
            ClearTween();
            _tweenAnimation = DOTween.Sequence();
            var enemyIndex = MainModel.BattleSampleData.currentEnemyIndex;
            var targets = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData[enemyIndex]
                .targets;
            var customViews = new List<Text>();
            for (var i = 0; i < targets.Count; i++)
            {
                var customView = SetupView(targets[i]);
                customViews.Add(customView);
                var color = customView.color;
                var tweenX = customView.transform.DOMoveX(AbilityAnimationConfig.PlusShiftX,
                    AbilityAnimationConfig.Duration);
                var tweenColor = customView.DOColor(new Color(color.r, color.g, color.b, 0),
                    AbilityAnimationConfig.Duration);
                _tweenAnimation.Join(tweenX);
                _tweenAnimation.Join(tweenColor);
            }

            _tweenAnimation.OnComplete(() =>
            {
                foreach (var view in customViews) Destroy(view);
                Observer.Emit(BattleSampleEvent.PlayEnemyOffensiveAbilityAnimationComplete);
            });
            _tweenAnimation.Play();
        }

        private Text SetupView(int targetIndex)
        {
            var customView = Instantiate(_heroDamageTakenViews[targetIndex],
                _heroDamageTakenViews[targetIndex].transform.parent);
            var hurtValue = MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData[targetIndex].hurtValue;
            var abilityHurt =
                MainModel.BattleSampleData.primitivePlayer.battleSampleHeroesData[targetIndex].abilityHurt;
            customView.gameObject.SetActive(hurtValue >= 0);
            customView.text = hurtValue.ToString();
            switch (abilityHurt.type)
            {
                case AbilityType.EnemySimpleAttack:
                    customView.color = Color.red;
                    break;
                case AbilityType.EnemyBleedAttack:
                    customView.color = Color.red;
                    customView.text = "Bleed";
                    break;
                case AbilityType.EnemyDevourAttack:
                    customView.color = Color.cyan;
                    break;
                case AbilityType.EnemyMassAttack:
                    customView.color = Color.blue;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return customView;
        }

        private void HideViews()
        {
            foreach (var view in _heroDamageTakenViews) view.gameObject.SetActive(false);
        }
    }
}