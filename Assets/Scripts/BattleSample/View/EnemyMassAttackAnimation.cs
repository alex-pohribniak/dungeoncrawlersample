﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;

namespace BattleSample.View
{
    public class EnemyMassAttackAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayEnemyMassAttackAnimation;

        public AnimationViewLinks viewLinks;

        public EnemyMassAttackAnimation()
        {
            _onPlayEnemyMassAttackAnimation = OnPlayEnemyMassAttackAnimation;
        }

        private void OnPlayEnemyMassAttackAnimation()
        {
            duration = 1.0f;
            ClearAnimation();
            CreateSequence();
            var targetEnemy = viewLinks.DefineEnemy();
            var tweenShakeEnemy = targetEnemy.transform.DOShakePosition(duration, 10f, 10, 10f);
            Join(tweenShakeEnemy);
            spriteRenderer.gameObject.SetActive(true);
            LoadSprites("BattleSample/Sprites/AbilityVisual/Lightning");
            spriteRenderer.transform.position = targetEnemy.transform.position;
            var tweenLightning = DoSpriteAnimation(duration);
            var tweenShakeLightningOne = spriteRenderer.transform.DOShakePosition(duration, 1f, 0);
            var tweenShakeLightningTwo = spriteRenderer.transform.DOShakeScale(duration,1f,0);
            tweenLightning.OnUpdate(UpdateFrame);
            Join(tweenLightning);
            Join(tweenShakeLightningOne);
            Join(tweenShakeLightningTwo);
            OnComplete(() =>
            {
                ClearAnimation();
                spriteRenderer.gameObject.SetActive(false);
                Observer.Emit(BattleSampleEvent.PlayEnemyMassAttackAnimationComplete);
            });
            Play();
        }

        protected override void Awake()
        {
            base.Awake();
            spriteRenderer.gameObject.SetActive(false);
            Observer.AddListener(BattleSampleEvent.PlayEnemyMassAttackAnimation, _onPlayEnemyMassAttackAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.PlayEnemyMassAttackAnimation, _onPlayEnemyMassAttackAnimation);
        }
    }
}