﻿using Common.Singleton;
using UnityEngine;

namespace BattleSample.View
{
    public class AnimationViewLinks : MonoBehaviour
    {
        public HeroView heroView0;
        public HeroView heroView1;
        public HeroView heroView2;
        public HeroView heroView3;
        public GameObject soloEnemyCharacter0;
        public GameObject duoEnemyCharacter0;
        public GameObject duoEnemyCharacter1;
        public GameObject trioEnemyCharacter0;
        public GameObject trioEnemyCharacter1;
        public GameObject trioEnemyCharacter2;

        public HeroView DefineHero()
        {
            var targetHero = MainModel.BattleSampleData.CurrentHero().targetHero;
            if (targetHero == heroView0.index) return heroView0;
            if (targetHero == heroView1.index) return heroView1;
            if (targetHero == heroView2.index) return heroView2;
            return targetHero == heroView3.index ? heroView3 : null;
        }
        
        public HeroView DefineEnemyTargetHero()
        {
            var currentEnemy = MainModel.BattleSampleData.CurrentEnemy();
            var targetHero = currentEnemy.targets[0];
            if (targetHero == heroView0.index) return heroView0;
            if (targetHero == heroView1.index) return heroView1;
            if (targetHero == heroView2.index) return heroView2;
            return targetHero == heroView3.index ? heroView3 : null;
        }

        public GameObject DefineEnemy()
        {
            var count = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData.Count;
            var enemyIndex = MainModel.BattleSampleData.currentEnemyIndex;
            switch (count)
            {
                case 1:
                    return soloEnemyCharacter0;
                case 2:
                    switch (enemyIndex)
                    {
                        case 0:
                            return duoEnemyCharacter0;
                        case 1:
                            return duoEnemyCharacter1;
                    }

                    break;
                case 3:
                    switch (enemyIndex)
                    {
                        case 0:
                            return trioEnemyCharacter0;
                        case 1:
                            return trioEnemyCharacter1;
                        case 2:
                            return trioEnemyCharacter2;
                    }

                    break;
            }

            return default;
        }
    }
}