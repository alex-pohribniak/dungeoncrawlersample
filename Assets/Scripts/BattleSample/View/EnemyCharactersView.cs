﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using UnityEngine;

namespace BattleSample.View
{
    public class EnemyCharactersView : MonoBehaviour
    {
        private readonly SimpleDelegate _onRevealEnemyCharacters;
        private readonly SimpleDelegate _onUpdateEnemyCharactersView;

        public GameObject duo;
        public GameObject duoEnemy0;
        public GameObject duoEnemy1;
        public GameObject solo;
        public GameObject soloEnemy0;
        public GameObject trio;
        public GameObject trioEnemy0;
        public GameObject trioEnemy1;
        public GameObject trioEnemy2;

        public EnemyCharactersView()
        {
            _onUpdateEnemyCharactersView = OnUpdateEnemyCharactersView;
            _onRevealEnemyCharacters = OnRevealEnemyCharacters;
        }

        private void OnUpdateEnemyCharactersView()
        {
            var enemies = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData;
            for (var i = 0; i < enemies.Count; i++)
                switch (i)
                {
                    case 0 when !enemies[i].alive:
                        soloEnemy0.SetActive(false);
                        duoEnemy0.SetActive(false);
                        trioEnemy0.SetActive(false);
                        break;
                    case 1 when !enemies[i].alive:
                        duoEnemy1.SetActive(false);
                        trioEnemy1.SetActive(false);
                        break;
                    case 2 when !enemies[i].alive:
                        trioEnemy2.SetActive(false);
                        break;
                }
        }

        private void OnRevealEnemyCharacters()
        {
            Reveal();
        }

        private void Reveal()
        {
            var enemiesCount = MainModel.BattleSampleData.primitiveEncounterEnemy.battleSampleEnemiesData.Count;
            switch (enemiesCount)
            {
                case 1:
                    solo.gameObject.SetActive(true);
                    break;
                case 2:
                    duo.gameObject.SetActive(true);
                    break;
                case 3:
                    trio.gameObject.SetActive(true);
                    break;
            }
        }

        private void Awake()
        {
            Observer.AddListener(BattleSampleEvent.RevealEnemyCharacters, _onRevealEnemyCharacters);
            Observer.AddListener(BattleSampleEvent.UpdateEnemyCharactersView, _onUpdateEnemyCharactersView);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(BattleSampleEvent.RevealEnemyCharacters, _onRevealEnemyCharacters);
            Observer.RemoveListener(BattleSampleEvent.UpdateEnemyCharactersView, _onUpdateEnemyCharactersView);
        }
    }
}