﻿using BattleSample.Enums;
using Common.Enums;
using Common.Singleton;
using Common.Utils;
using DG.Tweening;
using UnityEngine;

namespace BattleSample.View
{
    public class TauntAnimation : TweenAnimation
    {
        private readonly SimpleDelegate _onPlayTauntAnimation;

        public GameObject tauntEffect;
        public ParticleSystem tauntWave;
        public AnimationViewLinks viewLinks;
        
        public TauntAnimation()
        {
            _onPlayTauntAnimation = OnPlayTauntAnimation;
        }

        private void OnPlayTauntAnimation()
        {
            duration = 1.0f;
            var halfDuration = duration / 2;
            ClearAnimation();
            CreateSequence();
            var targetEnemy = viewLinks.DefineEnemy();
            var targetPosition = targetEnemy.transform.position;
            var tweenShake = targetEnemy.transform.DOShakePosition(halfDuration, 10f, 10, 10f);
            Join(tweenShake);
            var oXVector = new Vector3(1f, 0f, 0f);
            var angle = Vector3.Angle(oXVector, targetPosition - tauntWave.transform.position) - 90f;
            var tauntWaveVector = new Vector3(0f, 0f, angle);
            var tweenRotate = tauntWave.transform.DORotate(tauntWaveVector, 0f);
            Join(tweenRotate);
            var tauntClone = Instantiate(tauntEffect, tauntEffect.transform.parent);
            tauntClone.SetActive(true);
            tauntClone.transform.position = targetPosition;
            var spriteRdr = tauntClone.GetComponent<SpriteRenderer>();
            var tweenFade = spriteRdr.DOFade(1f, duration);
            tweenFade.OnComplete(tweenFade.PlayBackwards);
            Append(tweenFade);
            OnComplete(() =>
            {
                Destroy(tauntClone);
                tauntWave.Stop();
                ClearAnimation();
                Observer.Emit(BattleSampleEvent.PlayTauntAnimationComplete);
            });
            Play();
            tauntWave.Play();
        }
        
        protected override void Awake()
        {
            base.Awake();
            tauntEffect.SetActive(false);
            Observer.AddListener(BattleSampleEvent.PlayTauntAnimation, _onPlayTauntAnimation);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (Observer.IsNull())
            {
                return;
            }
            Observer.RemoveListener(BattleSampleEvent.PlayTauntAnimation, _onPlayTauntAnimation);
        }
    }
}
