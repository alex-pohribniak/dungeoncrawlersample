﻿namespace BattleSample.Config
{
    public static class AbilityAnimationConfig
    {
        public const int MinusShiftX = -200;
        public const int PlusShiftX = 200;
        public const float Duration = 1.5f;
    }
}