﻿using System;
using DungeonCrawlerSample.Model;
using Game.Model;

namespace BattleSample.Model
{
    [Serializable]
    public class BattleSampleData
    {
        public int currentEnemyIndex;
        public int currentHeroIndex;

        public PrimitiveEnemyData primitiveEncounterEnemy;
        public PrimitivePlayerData primitivePlayer;

        public BattleSampleHeroData CurrentHero()
        {
            return primitivePlayer.battleSampleHeroesData[currentHeroIndex];
        }

        public BattleSampleEnemyData TargetEnemy()
        {
            return primitiveEncounterEnemy.battleSampleEnemiesData[CurrentHero().targetEnemy];
        }

        public BattleSampleEnemyData CurrentEnemy()
        {
            return primitiveEncounterEnemy.battleSampleEnemiesData[currentEnemyIndex];
        }
    }
}