﻿using System;
using System.Collections.Generic;
using Common.Enums;

namespace BattleSample.Model
{
    [Serializable]
    public class BattleSampleEnemyData
    {
        public bool active;
        public bool alive;
        public bool taunted;
        public int tauntDuration;
        public EnemyType type;
        public List<AbilityData> abilities;
        public int nextAbility;
        public List<int> targets;
        public int healthPoints;
        public int maxHealthPoints;
        public int armor;
        public int maxArmor;
        public AbilityData abilityHurt;
        public int hurtValue;
        
        public AbilityData CurrentAbility()
        {
            return abilities[nextAbility];
        }

        public bool Taunted()
        {
            return tauntDuration > 0;
        }
    }
}