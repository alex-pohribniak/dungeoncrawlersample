﻿using System;
using BattleSample.Enums;

namespace BattleSample.Model
{
    [Serializable]
    public class AbilityData
    {
        public AbilityType type;
        public int coolDown;
        public int coolDownTotal;
        public int power;
        public int powerOverTime;

        public bool Active()
        {
            return coolDown == 0;
        }
    }
}