﻿using System;
using System.Collections.Generic;
using Common.Enums;

namespace BattleSample.Model
{
    [Serializable]
    public class BattleSampleHeroData
    {
        public bool active;
        public bool defended;
        public bool enemyTarget;
        public bool alive;
        public HeroType type;
        public List<AbilityData> abilities;
        public int selectedAbility;
        public int targetHero;
        public int targetEnemy;
        public int healthPoints;
        public int maxHealthPoints;
        public int armor;
        public int maxArmor;
        public AbilityData abilityHurt;
        public int hurtValue;
        public bool bleed;
        public int bleedDuration;
        public int bleedValue;
        
        public AbilityData CurrentAbility()
        {
            return abilities[selectedAbility];
        }
    }
}